package space.rymiel.shinkorobi.ext.ntp.paper;

import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.ext.NTPExtension;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;

import java.util.Objects;

public record JoinListener(
    ShinKorobiPaper plugin, NTPExtension extension, LoggingPlugin log, PaperListeners listeners)
    implements Listener {
  @EventHandler(priority = EventPriority.HIGHEST)
  public void onPlayerJoin(PlayerJoinEvent playerJoinEvent) {
    var p = playerJoinEvent.getPlayer();
    plugin.registry().dispatch(
        new NTPExtension.Message.AskPending(p.getUniqueId()),
        res -> {
          var in = ByteStreams.newDataInput(res.body());
          var targetWorld = in.readUTF();
          var useExact = in.readBoolean();
          var x = in.readDouble();
          var y = in.readDouble();
          var z = in.readDouble();

          if (!targetWorld.isEmpty()) {
            var world = Objects.requireNonNull(Bukkit.getWorld(targetWorld));
            Location loc;
            if (targetWorld.equals(p.getWorld().getName()) && !useExact) {
              return; // no teleportation needed
            } else if (useExact) {
              loc = new Location(world, x, y ,z);
            } else {
              loc = world.getSpawnLocation();
            }
            Bukkit.getScheduler().runTask(plugin, () -> p.teleport(loc));
          }
        }
    );
  }
}
