package space.rymiel.shinkorobi.ext.ntp.paper;

import org.bukkit.Bukkit;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.ext.NTPExtension;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;

public record PaperListeners(NTPExtension ext, LoggingPlugin log, ShinKorobiPaper parent) {
  public void register() {
    Bukkit.getPluginManager().registerEvents(new JoinListener(parent, ext, log, this), parent);
  }
}
