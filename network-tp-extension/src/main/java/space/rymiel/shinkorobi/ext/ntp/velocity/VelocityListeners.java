package space.rymiel.shinkorobi.ext.ntp.velocity;

import com.google.common.io.ByteArrayDataOutput;
import space.rymiel.shinkorobi.api.MessageListener;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiCommunication;
import space.rymiel.shinkorobi.ext.NTPExtension;
import space.rymiel.shinkorobi.taka.ShinKorobiVelocity;

import java.util.HashMap;
import java.util.UUID;

public record VelocityListeners(NTPExtension ext, LoggingPlugin log, ShinKorobiVelocity parent) {
  private static final HashMap<String, HashMap<UUID, LocationWrapper>> pending = new HashMap<>();

  private record LocationWrapper(String world, boolean exact, double x, double y, double z) {}

  private LocationWrapper getPendingWorld(String server, UUID player) {
    var serverMap = pending.getOrDefault(server, new HashMap<>());
    return serverMap.getOrDefault(player, new LocationWrapper("", false, 0, 0, 0));
  }

  private void setPendingWorld(String server, UUID player, LocationWrapper world) {
    var serverMap = pending.getOrDefault(server, new HashMap<>());
    serverMap.put(player, world);
    pending.put(server, serverMap);
  }

  private void deletePendingWorld(String server, UUID player) {
    var serverMap = pending.getOrDefault(server, new HashMap<>());
    serverMap.remove(player);
    pending.put(server, serverMap);
  }

  @MessageListener
  public void onNegotiate(NTPExtension.Message.Negotiate msg) {
    var server = msg.targetServer();
    var world = msg.targetWorld();
    var uuid = msg.player();
    var player = parent.getServer().getPlayer(uuid).orElseThrow();

    setPendingWorld(server, uuid, new LocationWrapper(world, msg.useExact(), msg.x(), msg.y(), msg.z()));

    player.createConnectionRequest(parent.getServer().getServer(server).orElseThrow()).connectWithIndication().thenAcceptAsync((result -> {
      if (!result) {
        deletePendingWorld(server, uuid);
      }
    }));
  }

  @MessageListener
  public void onAskPending(NTPExtension.Message.AskPending msg, KorobiCommunication com, ByteArrayDataOutput res) {
    log.debug("%s", pending);

    var server = parent.registry().resolveServerHash(com.sender());
    var uuid = msg.player();

    var match = getPendingWorld(server, uuid);
    deletePendingWorld(server, uuid);

    res.writeUTF(match.world());
    res.writeBoolean(match.exact());
    res.writeDouble(match.x());
    res.writeDouble(match.y());
    res.writeDouble(match.z());
  }
}
