package space.rymiel.shinkorobi.ext;

import cloud.commandframework.arguments.standard.DoubleArgument;
import cloud.commandframework.arguments.standard.StringArgument;
import cloud.commandframework.bukkit.parsers.PlayerArgument;
import cloud.commandframework.paper.PaperCommandManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import space.rymiel.shinkorobi.api.AMessage;
import space.rymiel.shinkorobi.api.KorobiExtension;
import space.rymiel.shinkorobi.common.provider.common.IMessageEnum;
import space.rymiel.shinkorobi.ext.ntp.velocity.VelocityListeners;
import space.rymiel.shinkorobi.ext.ntp.paper.PaperListeners;
import space.rymiel.shinkorobi.hiku.PaperMessengerProvider;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;
import space.rymiel.shinkorobi.taka.VelocityMessengerProvider;
import space.rymiel.shinkorobi.taka.ShinKorobiVelocity;

import java.util.UUID;

@SuppressWarnings("unused")
public class NTPExtension extends KorobiExtension {
  public NTPExtension() {
    super("ntp");
  }

  @Override
  public void onEnable() {
    if (parent.messengerProvider() instanceof PaperMessengerProvider) {
      var korobiPaper = (ShinKorobiPaper) parent;
      log.info("We're on paper! %s", korobiPaper);

      PaperCommandManager<CommandSender> cmd = korobiPaper.commandManager();
      cmd.command(
          cmd.commandBuilder("korobi")
              .literal("ntp")
              .argument(PlayerArgument.of("player"))
              .argument(StringArgument.of("server"))
              .argument(StringArgument.of("world"))
              .argument(DoubleArgument.optional("x"))
              .argument(DoubleArgument.optional("y"))
              .argument(DoubleArgument.optional("z"))
          .handler((ctx) -> {
            boolean useExact = ctx.contains("x") && ctx.contains("y") && ctx.contains("z");
            var message = useExact ?
                new Message.Negotiate(((Player) ctx.get("player")).getUniqueId(), ctx.get("server"), ctx.get("world"), true, ctx.get("x"), ctx.get("y"), ctx.get("z")) :
                new Message.Negotiate(((Player) ctx.get("player")).getUniqueId(), ctx.get("server"), ctx.get("world"), false, 0.0d, 0.0d, 0.0d);
            parent.registry().dispatch(message);
          })
      );

      var listeners = new PaperListeners(this, log, korobiPaper);
      listeners.register();

      register(Message.class).declareAll(Message.class);
    } else if (parent.messengerProvider() instanceof VelocityMessengerProvider) {
      var korobiVelocity = (ShinKorobiVelocity) parent;
      log.info("We're on velocity! %s", korobiVelocity);

      var listeners = new VelocityListeners(this, log, korobiVelocity);

      register(Message.class).listenAll(listeners);
    } else {
      log.fatal("Unknown platform %s %s", parent.getClass(), parent);
    }
  }

  public enum Message implements IMessageEnum {
    NEGOTIATE, ASK_PENDING;

    @AMessage(i = Message.class, name = "NEGOTIATE")
    public record Negotiate(UUID player, String targetServer, String targetWorld, boolean useExact, double x, double y, double z) {}

    @AMessage(i = Message.class, name = "ASK_PENDING")
    public record AskPending(UUID player) {}
  }
}
