
description = "korobi-network-tp-extension"

dependencies {
    compileOnly(project(":korobi"))
    compileOnly("io.papermc.paper:paper-api:1.19-R0.1-SNAPSHOT")
    compileOnly("com.velocitypowered", "velocity-api", "3.1.1")
    compileOnly("cloud.commandframework:cloud-paper:1.8.3")
}
