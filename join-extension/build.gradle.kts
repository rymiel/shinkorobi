
description = "korobi-join-extension"

dependencies {
    implementation("org.spongepowered", "configurate-yaml", "4.1.2")
    implementation("com.h2database", "h2-mvstore", "2.1.210")
    compileOnly(project(":korobi"))
    compileOnly("io.papermc.paper:paper-api:1.19-R0.1-SNAPSHOT")
    compileOnly("com.velocitypowered", "velocity-api", "3.1.1")
    compileOnly("net.essentialsx:EssentialsX:2.19.0-SNAPSHOT")
    compileOnly("com.github.LeonMangler:SuperVanish:6.2.0") {
        isTransitive = false
    }
}
