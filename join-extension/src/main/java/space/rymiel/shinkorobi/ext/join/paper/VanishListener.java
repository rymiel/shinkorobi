package space.rymiel.shinkorobi.ext.join.paper;

import de.myzelyam.api.vanish.PlayerHideEvent;
import de.myzelyam.api.vanish.PlayerShowEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import space.rymiel.shinkorobi.ext.JoinExtension;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;

public record VanishListener(
    ShinKorobiPaper plugin, JoinExtension extension, PaperListeners listeners) implements Listener {
  @EventHandler(priority = EventPriority.MONITOR)
  public void onVanish(PlayerHideEvent playerHideEvent) {
    listeners.knownVanish().add(playerHideEvent.getPlayer().getUniqueId());
    if (playerHideEvent.isSilent()) return;
    Player p = playerHideEvent.getPlayer();
    plugin
        .registry()
        .dispatch(
            new JoinExtension.Message.QuitWithDisplayName(
                p.getUniqueId(), p.displayName(), false, true));
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onUnVanish(PlayerShowEvent playerShowEvent) {
    plugin.debug(
        "%s %s %s", playerShowEvent.getPlayer(), playerShowEvent, playerShowEvent.isSilent());
    listeners.knownVanish().remove(playerShowEvent.getPlayer().getUniqueId());
    if (playerShowEvent.isSilent()) return;
    Player p = playerShowEvent.getPlayer();
    plugin
        .registry()
        .dispatch(
            new JoinExtension.Message.JoinWithDisplayName(
                p.getUniqueId(), p.displayName(), false, true, false));
  }
}
