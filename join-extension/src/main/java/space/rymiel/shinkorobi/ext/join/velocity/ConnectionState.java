package space.rymiel.shinkorobi.ext.join.velocity;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.ComponentLike;
import net.kyori.adventure.text.format.NamedTextColor;
import org.jetbrains.annotations.NotNull;

public enum ConnectionState implements ComponentLike {
  UNKNOWN(NamedTextColor.DARK_PURPLE),
  CONNECTING(NamedTextColor.YELLOW),
  CONNECTED(NamedTextColor.GREEN),
  DISCONNECTING(NamedTextColor.GOLD),
  DISCONNECTED(NamedTextColor.RED);

  private final NamedTextColor color;

  ConnectionState(NamedTextColor color) {
    this.color = color;
  }

  @Override
  public @NotNull Component asComponent() {
    return Component.text(this.name()).color(this.color);
  }
}
