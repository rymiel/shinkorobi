package space.rymiel.shinkorobi.ext.join.velocity;

import com.velocitypowered.api.event.PostOrder;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.DisconnectEvent;
import com.velocitypowered.api.event.connection.PostLoginEvent;
import com.velocitypowered.api.proxy.Player;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.minimessage.MiniMessage;
import org.h2.mvstore.MVMap;
import net.kyori.adventure.text.minimessage.tag.resolver.Placeholder;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.spongepowered.configurate.serialize.SerializationException;
import space.rymiel.shinkorobi.api.MessageListener;
import space.rymiel.shinkorobi.common.ConfigurationManager;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.ext.JoinExtension;
import space.rymiel.shinkorobi.taka.ShinKorobiVelocity;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static net.kyori.adventure.text.Component.text;

public final class VelocityListeners {
  private static final Map<UUID, ConnectionState> connectionStateMap = new HashMap<>();
  private static final Map<UUID, String> knownUsernameMap = new HashMap<>();
  private final MVMap<UUID, String> lastSeenUsernameMap;
  private final LoggingPlugin log;
  private final ShinKorobiVelocity parent;
  private JoinConfig config;

  public VelocityListeners(LoggingPlugin log, ShinKorobiVelocity parent) {
    this.log = log;
    this.parent = parent;
    this.lastSeenUsernameMap = parent.store().openMap("lastSeenUsername");
    try {
      this.config = ConfigurationManager.objectFactory.get(JoinConfig.class).load(parent.configNode().node("join"));
    } catch (SerializationException e) {
      e.printStackTrace();
    }
  }

  public void register() {
    parent.getServer().getEventManager().register(parent, this);
  }

  @Subscribe(order = PostOrder.FIRST)
  public void connectedEvent(PostLoginEvent event) {
    rememberName(event.getPlayer().getUniqueId(), event.getPlayer().getUsername());
    setState(event.getPlayer().getUniqueId(), ConnectionState.CONNECTING);
  }

  @Subscribe(order = PostOrder.FIRST)
  public void disconnectedEvent(DisconnectEvent event) {
    setState(event.getPlayer().getUniqueId(), ConnectionState.DISCONNECTING);
  }

  @MessageListener
  public void onJoinMessage(JoinExtension.Message.JoinWithDisplayName msg) {
    JoinExtension.runChainCallbacks(msg);

    String rawMessage = config.messages().join();
    var player = parent.getServer().getPlayer(msg.uuid());
    var seenName = this.lastSeenUsernameMap.get(msg.uuid());
    var realName = player.map(Player::getUsername).orElse(null);
    player.ifPresent(value -> rememberName(msg.uuid(), value.getUsername()));
    boolean canBeFirstJoin = seenName == null;
    if (getState(msg.uuid()).equals(ConnectionState.CONNECTING) || msg.forced()) {
      if (seenName != null && !seenName.equals(realName)) rawMessage = config.messages().joinChanged();
      if (canBeFirstJoin && msg.firstJoin()) rawMessage = config.messages().joinFirst();
      if (msg.silent()) rawMessage = config.messages().joinSilent();
      var parsed = MiniMessage.miniMessage().deserialize(rawMessage,
        Placeholder.component("displayname", msg.displayName()),
        Placeholder.unparsed("last_name", Objects.requireNonNullElse(seenName, ""))
      );
      if (msg.silent()) {
        player.ifPresent(value -> value.sendMessage(parsed));
      } else if (msg.forced()) {
        parent.getServer().sendMessage(parsed);
      } else {
        player.ifPresent(value -> parent
            .getServer()
            .getScheduler()
            .buildTask(
                parent,
                () -> value.sendMessage(parsed))
            .delay(500, TimeUnit.MILLISECONDS)
            .schedule());
        parent
            .getServer()
            .getAllPlayers()
            .stream()
            .filter(e -> player.isEmpty() || !e.equals(player.get()))
            .forEach(p -> p.sendMessage(parsed));
        parent.getServer().getConsoleCommandSource().sendMessage(parsed);
      }
      if (!msg.silent() && realName != null) {
        this.lastSeenUsernameMap.put(msg.uuid(), realName);
        this.lastSeenUsernameMap.store.commit();
      }
      setState(msg.uuid(), ConnectionState.CONNECTED);
    }
  }

  @MessageListener
  public void onQuitMessage(JoinExtension.Message.QuitWithDisplayName msg) {
    String rawMessage = config.messages().quit();
    var parsed = MiniMessage.miniMessage().deserialize(rawMessage, Placeholder.component("displayname", msg.displayName()));

    if (getState(msg.uuid()).equals(ConnectionState.DISCONNECTING) || msg.forced()) {
      if (!msg.silent()) parent.getServer().sendMessage(parsed);
      setState(msg.uuid(), ConnectionState.DISCONNECTED);
    }
  }

  public ConnectionState getState(UUID uuid) {
    return connectionStateMap.getOrDefault(uuid, ConnectionState.UNKNOWN);
  }

  public void setState(UUID uuid, ConnectionState state) {
    var oldState = getState(uuid);
    if (oldState.equals(state)) return;
    final String username = knownUsernameMap.getOrDefault(uuid, "(unknown %s)".formatted(uuid));
    final Component component = Component.empty()
        .append(text(username).color(NamedTextColor.AQUA))
        .append(text(" ["))
        .append(oldState)
        .append(text("] -> ["))
        .append(state)
        .append(text("]"));
    log.info(LegacyComponentSerializer.legacySection().serialize(component));
    connectionStateMap.put(uuid, state);
  }

  public void rememberName(UUID uuid, String username) {
    knownUsernameMap.put(uuid, username);
  }
}
