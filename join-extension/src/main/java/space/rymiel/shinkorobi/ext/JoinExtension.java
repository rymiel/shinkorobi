package space.rymiel.shinkorobi.ext;

import net.kyori.adventure.text.Component;
import space.rymiel.shinkorobi.api.AMessage;
import space.rymiel.shinkorobi.api.KorobiExtension;
import space.rymiel.shinkorobi.common.provider.common.IMessageEnum;
import space.rymiel.shinkorobi.ext.join.paper.PaperListeners;
import space.rymiel.shinkorobi.ext.join.velocity.VelocityListeners;
import space.rymiel.shinkorobi.hiku.PaperMessengerProvider;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;
import space.rymiel.shinkorobi.taka.ShinKorobiVelocity;
import space.rymiel.shinkorobi.taka.VelocityMessengerProvider;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

public class JoinExtension extends KorobiExtension {
  private static List<Consumer<Message.JoinWithDisplayName>> chainCallbacks = new LinkedList<>();

  public JoinExtension() {
    super("join");
  }

  public static void addChainCallback(Consumer<Message.JoinWithDisplayName> callback) {
    chainCallbacks.add(callback);
  }

  public static void runChainCallbacks(Message.JoinWithDisplayName message) {
    for (var callback : chainCallbacks)
      callback.accept(message);
  }

  @Override
  public void onEnable() {
    if (parent.messengerProvider() instanceof PaperMessengerProvider) {
      var korobiPaper = (ShinKorobiPaper) parent;
      log.info("We're on paper! %s", korobiPaper);

      var listeners = new PaperListeners(this, this.log, korobiPaper);
      listeners.register();

      register(Message.class).declareAll(Message.class);
    } else if (parent.messengerProvider() instanceof VelocityMessengerProvider) {
      var korobiVelocity = (ShinKorobiVelocity) parent;
      log.info("We're on velocity! %s", korobiVelocity);

      var listeners = new VelocityListeners(this.log, korobiVelocity);
      listeners.register();

      register(Message.class).listenAll(listeners);
    } else {
      log.fatal("Unknown platform %s %s", parent.getClass(), parent);
    }
  }

  public enum Message implements IMessageEnum {
    JOIN_WITH_DISPLAY_NAME,
    QUIT_WITH_DISPLAY_NAME;

    @AMessage(i = Message.class, name = "JOIN_WITH_DISPLAY_NAME")
    public record JoinWithDisplayName(
        UUID uuid, Component displayName, boolean silent, boolean forced, boolean firstJoin) {
    }

    @AMessage(i = Message.class, name = "QUIT_WITH_DISPLAY_NAME")
    public record QuitWithDisplayName(
        UUID uuid, Component displayName, boolean silent, boolean forced) {
    }
  }
}
