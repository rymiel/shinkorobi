package space.rymiel.shinkorobi.ext.join.paper;

import net.essentialsx.api.v2.events.AsyncUserDataLoadEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import space.rymiel.shinkorobi.common.RegistryPlugin;
import space.rymiel.shinkorobi.ext.JoinExtension;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;

public record JoinListener(
    ShinKorobiPaper plugin, JoinExtension extension, PaperListeners listeners, long delay)
    implements Listener {
  @EventHandler(priority = EventPriority.HIGHEST)
  public void onPlayerJoin(PlayerJoinEvent playerJoinEvent) {
    final boolean isFirst = !playerJoinEvent.getPlayer().hasPlayedBefore();
    if (delay != 0) {
      Bukkit.getScheduler().runTaskLater(plugin, () -> playerJoinImpl(playerJoinEvent, isFirst), delay);
    } else {
      playerJoinImpl(playerJoinEvent, isFirst);
    }
  }

  private void playerJoinImpl(PlayerJoinEvent playerJoinEvent, boolean isFirst) {
    Player p = playerJoinEvent.getPlayer();
    final boolean isSilent = listeners.isVanished(playerJoinEvent.getPlayer());

    var msg = new JoinExtension.Message.JoinWithDisplayName(
        p.getUniqueId(), p.displayName(), isSilent, false, isFirst);
    plugin.registry().dispatch(msg);
    JoinExtension.runChainCallbacks(msg);
    playerJoinEvent.joinMessage(null);
  }

  @EventHandler(priority = EventPriority.HIGHEST)
  public void onPlayerQuit(PlayerQuitEvent playerQuitEvent) {
    Player p = playerQuitEvent.getPlayer();
    var isSilent = listeners.isVanished(playerQuitEvent.getPlayer());

    plugin
        .registry()
        .dispatch(
            new JoinExtension.Message.QuitWithDisplayName(
                p.getUniqueId(), p.displayName(), isSilent, false));
    playerQuitEvent.quitMessage(null);
  }

  public record Delayed(RegistryPlugin plugin, PaperListeners listeners) implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerJoin(AsyncUserDataLoadEvent playerJoinEvent) {
      Player p = playerJoinEvent.getUser().getBase();
      final boolean isSilent = listeners.isVanished(p);
      final boolean isFirst = !p.hasPlayedBefore();

      var msg = new JoinExtension.Message.JoinWithDisplayName(
          p.getUniqueId(), p.displayName(), isSilent, false, isFirst);
      plugin.registry().dispatch(msg);
      JoinExtension.runChainCallbacks(msg);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent playerQuitEvent) {
      Player p = playerQuitEvent.getPlayer();
      var isSilent = listeners.isVanished(playerQuitEvent.getPlayer());
      plugin
          .registry()
          .dispatch(
              new JoinExtension.Message.QuitWithDisplayName(
                  p.getUniqueId(), p.displayName(), isSilent, false));
      playerQuitEvent.quitMessage(null);
    }
  }
}
