package space.rymiel.shinkorobi.ext.join.paper;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerLoadEvent;
import space.rymiel.shinkorobi.ext.JoinExtension;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;

public record StartupListener(
    ShinKorobiPaper plugin, JoinExtension extension, PaperListeners listeners) implements Listener {
  @EventHandler
  public void onServerLoad(ServerLoadEvent serverLoadEvent) {
    if (Bukkit.getPluginManager().isPluginEnabled("SuperVanish")
        || Bukkit.getPluginManager().isPluginEnabled("PremiumVanish")) {
      plugin.info("Using vanish listener");
      Bukkit.getPluginManager()
          .registerEvents(new VanishListener(plugin, extension, listeners), plugin);
      plugin.info("Registered");
    }
  }
}
