package space.rymiel.shinkorobi.ext.join.velocity;

import org.spongepowered.configurate.objectmapping.ConfigSerializable;

@ConfigSerializable
public record JoinConfig(
    JoinConfigMessages messages
) {
  @ConfigSerializable
  record JoinConfigMessages(
    String join,
    String joinSilent,
    String joinChanged,
    String joinFirst,
    String quit
  ) {
  }
}
