package space.rymiel.shinkorobi.ext.join.paper;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.ext.JoinExtension;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public record PaperListeners(JoinExtension ext, LoggingPlugin log, ShinKorobiPaper parent) {
  private static final Set<UUID> knownVanish = new HashSet<>();

  public Set<UUID> knownVanish() {
    return knownVanish;
  }

  public void register() {
    if (Bukkit.getPluginManager().isPluginEnabled("Essentials")) {
      try {
        log.info("Attempting to use new EssentialsX async join data event");
        Class.forName("net.essentialsx.api.v2.events.AsyncUserDataLoadEvent");
        Bukkit.getPluginManager().registerEvents(new JoinListener.Delayed(parent, this), parent);
        log.info("Success!");
      } catch (ClassNotFoundException e) {
        log.warn("Could not use EssentialsX async event, using delayed join event");
        Bukkit.getPluginManager().registerEvents(new JoinListener(parent, ext, this, 3), parent);
      }
    } else {
      log.info("Using direct join event");
      Bukkit.getPluginManager().registerEvents(new JoinListener(parent, ext, this, 0), parent);
    }

    Bukkit.getPluginManager().registerEvents(new StartupListener(parent, ext, this), parent);
  }

  public boolean isVanished(Player player) {
    var val = isVanishedImpl(player);
    log.debug("Vanish check for %s: %s", player, val);
    log.debug("Known vanish cache set %s", knownVanish);
    return val;
  }

  private boolean isVanishedImpl(Player player) {
    if (knownVanish.contains(player.getUniqueId())) return true;
    for (MetadataValue meta : player.getMetadata("vanished")) {
      if (meta.asBoolean()) {
        knownVanish.add(player.getUniqueId());
        return true;
      }
    }
    return false;
  }
}
