package space.rymiel.shinkorobi.api;

import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.common.MessagingClient;
import space.rymiel.shinkorobi.common.RegistryPlugin;
import space.rymiel.shinkorobi.common.messenger.registry.ProviderRegistry;
import space.rymiel.shinkorobi.common.messenger.socket.SocketClient;
import space.rymiel.shinkorobi.common.provider.common.AbstractProvider;
import space.rymiel.shinkorobi.common.provider.common.IMessageEnum;

public abstract class KorobiExtension {
  protected final String identifier;
  protected LoggingPlugin log;
  protected RegistryPlugin parent;

  public KorobiExtension(String identifier) {
    this.identifier = identifier;
  }

  public void setLogger(LoggingPlugin log) {
    this.log = log;
  }

  public void setParent(RegistryPlugin parent) {
    this.parent = parent;
  }

  public String getIdentifier() {
    return identifier;
  }

  public abstract void onEnable();

  public void onReloadConfig() {}

  public static class ExtensionProvider extends AbstractProvider {
    private final Class<?> registeringClass;

    public Class<?> registeringClass() {
      return registeringClass;
    }

    protected ExtensionProvider(SocketClient defaultClient, Class<? extends IMessageEnum> messages, String name, Class<?> registeringClass) {
      super(defaultClient, messages, name);
      this.registeringClass = registeringClass;
    }
  }

  protected ProviderRegistry.Entry.Builder register(Class<? extends IMessageEnum> messages) {
    SocketClient defaultClient = null;
    if (parent instanceof MessagingClient messagingClient) {
      defaultClient = messagingClient.messenger();
    }
    var provider = new ExtensionProvider(defaultClient, messages, getIdentifier(), this.getClass());
    return parent.registry().build(provider);
  }
}
