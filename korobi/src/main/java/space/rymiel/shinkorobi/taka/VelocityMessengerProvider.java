package space.rymiel.shinkorobi.taka;

import org.jetbrains.annotations.NotNull;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.common.messenger.MessengerProvider;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class VelocityMessengerProvider extends MessengerProvider {
  private final ShinKorobiVelocity plugin;

  public VelocityMessengerProvider(ShinKorobiVelocity plugin) {
    this.plugin = plugin;
  }

  @Override
  public void asynchronously(Runnable action) {
    plugin.getServer().getScheduler().buildTask(this.plugin, action).schedule();
  }

  @Override
  public void later(Runnable action, long delayMs) {
    plugin
        .getServer()
        .getScheduler()
        .buildTask(this.plugin, action)
        .delay(delayMs, TimeUnit.MILLISECONDS)
        .schedule();
  }

  @Override
  public LoggingPlugin plugin() {
    return plugin;
  }

  @Override
  public @NotNull File dataFolder() {
    return plugin.getDataDirectory().toFile();
  }

  @Override
  public int hash() {
    return plugin.getServer().getVersion().hashCode();
  }
}
