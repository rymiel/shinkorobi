package space.rymiel.shinkorobi.taka.provider.builtin;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiCommunication;
import space.rymiel.shinkorobi.common.messenger.registry.ProviderRegistry;
import space.rymiel.shinkorobi.common.provider.BuiltinMessage;
import space.rymiel.shinkorobi.common.provider.common.AbstractProvider;
import space.rymiel.shinkorobi.taka.ShinKorobiVelocity;

public class KorobiProvider extends AbstractProvider {
  private ProviderRegistry registry = null;
  private final ShinKorobiVelocity plugin;

  public KorobiProvider(ShinKorobiVelocity plugin) {
    super(BuiltinMessage.class, "korobi");
    this.plugin = plugin;
  }

  public void register(ProviderRegistry registry) {
    this.registry = registry;
    registry
        .build(this)
        .listen(BuiltinMessage.CONFIG_RELOAD, this::reloadConfig)
        .listen(BuiltinMessage.HANDSHAKE, this::handshake);
  }

  private void handshake(KorobiCommunication m, ByteArrayDataOutput response) {
    var in = ByteStreams.newDataInput(m.body());
    String name = in.readUTF();
    String serverIp = in.readUTF();
    String connectionIp = m.address().getAddress().toString();
    int port = in.readInt();
    String customName = in.readUTF();
    String fullConnection = String.format("(%s)%s:%s (%s)", serverIp, connectionIp, port, name);
    this.plugin.info("Incoming §ahandshake§r from %s (%s)", fullConnection, customName);
    registry.addNameToServerHash(m.sender(), customName.isEmpty() ? fullConnection : customName);

    response.writeUTF(fullConnection);
  }

  private void reloadConfig(KorobiCommunication m, ByteArrayDataOutput response) {
    this.plugin.info("Instructed to reload config by %s", registry.resolveServerHash(m.sender()));
    this.plugin.reloadConfig();
  }
}
