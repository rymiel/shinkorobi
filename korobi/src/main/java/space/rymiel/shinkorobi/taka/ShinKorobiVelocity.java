package space.rymiel.shinkorobi.taka;

import com.google.inject.Inject;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.event.proxy.ProxyShutdownEvent;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.plugin.annotation.DataDirectory;
import com.velocitypowered.api.proxy.ProxyServer;
import io.leangen.geantyref.TypeToken;
import org.h2.mvstore.MVStore;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.spongepowered.configurate.ConfigurationNode;
import space.rymiel.shinkorobi.api.KorobiExtension;
import space.rymiel.shinkorobi.common.ConfigurationManager;
import space.rymiel.shinkorobi.common.GlobalLog;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.common.RegistryPlugin;
import space.rymiel.shinkorobi.common.extensions.ExtensionRegistry;
import space.rymiel.shinkorobi.common.messenger.MessengerProvider;
import space.rymiel.shinkorobi.common.messenger.registry.ProviderRegistry;
import space.rymiel.shinkorobi.common.messenger.socket.SocketServer;
import space.rymiel.shinkorobi.taka.provider.builtin.KorobiProvider;
import space.rymiel.shinkorobi.taka.provider.debug.DebugProvider;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Plugin(id = "shinkorobi", name = "ShinKorobi", version = "@version@", authors = {"rymiel"})
public final class ShinKorobiVelocity implements LoggingPlugin, RegistryPlugin {
  private final ProviderRegistry registry;
  private final ExtensionRegistry extensions = new ExtensionRegistry();
  private SocketServer messenger;
  private Config config;
  private final Logger logger;
  private final Path dataDirectory;
  private final ProxyServer server;
  private final MVStore store;
  private VelocityMessengerProvider messengerProvider;
  private ConfigurationNode configNode;

  public ProxyServer getServer() {
    return this.server;
  }

  public Path getDataDirectory() {
    return this.dataDirectory;
  }

  public ConfigurationNode configNode() {
    return this.configNode;
  }

  public ProviderRegistry registry() {
    return this.registry;
  }

  public MVStore store() {
    return this.store;
  }

  @Override
  public MessengerProvider messengerProvider() {
    return messengerProvider;
  }

  @Inject
  public ShinKorobiVelocity(ProxyServer server, Logger logger, @DataDirectory Path dataDirectory) {
    this.logger = logger;
    this.dataDirectory = dataDirectory;
    this.server = server;
    this.store = MVStore.open(dataDirectory.resolve("persistent.store").toString());
    this.registry = new ProviderRegistry(store.openMap("serverHashes"));
    reloadConfig();
  }

  @Subscribe
  public void onInitialize(ProxyInitializeEvent e) {
    GlobalLog.setLogger(this);

    new DebugProvider(this).register(registry);
    new KorobiProvider(this).register(registry);

    debug(this.config);
    messengerProvider = new VelocityMessengerProvider(this);
    messenger =
        SocketServer.Builder.build(
            messengerProvider,
            registry,
            config.socketServerBindTo(),
            config.socketServerPort(),
            config.socketServerSecretKey());

    extensions.loadExtensions(messengerProvider);

    info("ShinKorobi has finished loading, %s extensions present", extensions.count());
  }

  @Subscribe
  public void onDisable(ProxyShutdownEvent e) {
    messenger.closeIfRunning();
    store.commit();
  }

  @Override
  public boolean getDebugMode() {
    return config.debug();
  }

  @Override
  public void info(@NotNull Object msg, Object... ext) {
    this.logger.info(String.format(msg.toString(), ext));
  }

  @Override
  public void debug(@NotNull Object msg, Object... ext) {
    if (config.debug()) this.logger.info("[DEBUG] " + String.format(msg.toString(), ext));
  }

  @Override
  public void fatal(@NotNull Object msg, Object... ext) {
    this.logger.error(String.format(msg.toString(), ext));
  }

  @Override
  public void warn(@NotNull Object msg, Object... ext) {
    this.logger.warn(String.format(msg.toString(), ext));
  }

  public void reloadConfig() {
    try {
      Files.createDirectories(this.dataDirectory);
      var configManager = new ConfigurationManager<>(
          this.dataDirectory.resolve("config.yml").toFile(),
          TypeToken.get(Config.class),
          new Config()
      );
      this.configNode = configManager.saveDefault().load();
      this.config = configManager.loadConfig();
      this.extensions.forEach(KorobiExtension::onReloadConfig);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
