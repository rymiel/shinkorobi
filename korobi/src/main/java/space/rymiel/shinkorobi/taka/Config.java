package space.rymiel.shinkorobi.taka;

import org.spongepowered.configurate.objectmapping.ConfigSerializable;

@ConfigSerializable
public record Config(
    boolean debug,
    int socketServerPort,
    String socketServerBindTo,
    String socketServerSecretKey
) {
  public Config() {
    this(false, 9099, "127.0.0.1", "secret.key");
  }
}
