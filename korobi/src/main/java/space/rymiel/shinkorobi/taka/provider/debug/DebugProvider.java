package space.rymiel.shinkorobi.taka.provider.debug;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.kyori.adventure.text.Component;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiCommunication;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiMessage;
import space.rymiel.shinkorobi.common.messenger.registry.ProviderRegistry;
import space.rymiel.shinkorobi.common.provider.DebugMessage;
import space.rymiel.shinkorobi.common.provider.common.AbstractProvider;
import space.rymiel.shinkorobi.common.serializer.RecordSerializer;
import space.rymiel.shinkorobi.common.util.RegistryFormatter;
import space.rymiel.shinkorobi.taka.ShinKorobiVelocity;

import java.util.ArrayList;

public class DebugProvider extends AbstractProvider {
  private ProviderRegistry registry = null;
  private final ShinKorobiVelocity plugin;

  public DebugProvider(ShinKorobiVelocity plugin) {
    super(DebugMessage.class, "debug");
    this.plugin = plugin;
  }

  public void register(ProviderRegistry registry) {
    this.registry = registry;
    registry
        .build(this)
        .listen(DebugMessage.STARTUP, this::startupListener)
        .listen(DebugMessage.MANUAL, this::manualListener)
        .listen(DebugMessage.REGISTRY, this::registryListener)
        .consume(DebugMessage.PING);
  }

  private void startupListener(KorobiMessage m) {
    var in = ByteStreams.newDataInput(m.body());
    String reason = in.readUTF();
    this.plugin.info("Backend server initialized: %s", reason);
  }

  private void manualListener(KorobiMessage m) {
    var in = ByteStreams.newDataInput(m.body());
    String reason = in.readUTF();
    this.plugin.info("Received manually sent message: %s", reason);
  }

  private void registryListener(KorobiCommunication m, ByteArrayDataOutput out) {
    var map = registry.getRegistryMap();
    var format = new ArrayList<Component>();
    RegistryFormatter.formatRegistry(format, map);

    var r = new DebugMessage.RegistryResponse(format.toArray(new Component[0]));
    out.write(RecordSerializer.serializeRecord(r));
  }
}
