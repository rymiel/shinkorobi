package space.rymiel.shinkorobi.common;

import space.rymiel.shinkorobi.common.messenger.socket.SocketClient;

public interface MessagingClient {
  SocketClient messenger();
}
