package space.rymiel.shinkorobi.common.extensions;

import space.rymiel.shinkorobi.api.KorobiExtension;
import space.rymiel.shinkorobi.common.RegistryPlugin;
import space.rymiel.shinkorobi.common.messenger.MessengerProvider;
import space.rymiel.shinkorobi.common.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Objects;
import java.util.function.Consumer;

public class ExtensionRegistry {
  private final HashSet<KorobiExtension> extensions = new HashSet<>();

  public int count() { return extensions.size(); }

  public void forEach(Consumer<KorobiExtension> callback) {
    for (var ext : extensions) {
      callback.accept(ext);
    }
  }

  public void loadExtensions(MessengerProvider provider) {
    try {
      final File[] files = Objects.requireNonNull(new File(provider.dataFolder(), "extensions").listFiles((dir, name) -> name.endsWith(".jar")));
      final URL[] jars = new URL[files.length];
      for (int i = 0; i < files.length; i++) {
        jars[i] = files[i].toURI().toURL();
      }

      var classes = FileUtil.findClasses(jars, KorobiExtension.class);
      for (var cls : classes) {
        var ext = Objects.requireNonNull(FileUtil.getInstance(Objects.requireNonNull(cls)));
        extensions.add(ext);
      }
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
    }

    for (var ext : extensions) {
      ext.setLogger(new ExtensionLogger(ext.getIdentifier(), provider.plugin()));
      ext.setParent((RegistryPlugin) provider.plugin());
      ext.onEnable();
    }
  }
}
