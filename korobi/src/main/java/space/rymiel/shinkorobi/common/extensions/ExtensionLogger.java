package space.rymiel.shinkorobi.common.extensions;

import org.jetbrains.annotations.NotNull;
import space.rymiel.shinkorobi.common.LoggingPlugin;

public record ExtensionLogger(String name, LoggingPlugin parent) implements LoggingPlugin {
  public ExtensionLogger(String name, LoggingPlugin parent) {
    this.name = "[" + name + "] ";
    this.parent = parent;
  }

  @Override
  public boolean getDebugMode() {
    return parent.getDebugMode();
  }

  @Override
  public void info(@NotNull Object msg, Object... ext) {
    parent.info(this.name + msg, ext);
  }

  @Override
  public void debug(@NotNull Object msg, Object... ext) {
    parent.debug(this.name + msg, ext);
  }

  @Override
  public void fatal(@NotNull Object msg, Object... ext) {
    parent.fatal(this.name + msg, ext);
  }

  @Override
  public void warn(@NotNull Object msg, Object... ext) {
    parent.warn(this.name + msg, ext);
  }
}
