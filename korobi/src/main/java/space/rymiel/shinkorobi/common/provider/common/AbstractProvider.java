package space.rymiel.shinkorobi.common.provider.common;

import space.rymiel.shinkorobi.common.messenger.dispatcher.ProviderDispatcher;
import space.rymiel.shinkorobi.common.messenger.socket.SocketClient;

public abstract class AbstractProvider {
  public final String name;
  public final Class<? extends IMessageEnum> messages;
  private SocketClient defaultClient = null;

  protected AbstractProvider(Class<? extends IMessageEnum> messages, String name) {
    this.name = name;
    this.messages = messages;
  }

  protected AbstractProvider(
      SocketClient defaultClient, Class<? extends IMessageEnum> messages, String name) {
    this.defaultClient = defaultClient;
    this.messages = messages;
    this.name = name;
  }

  public void dispatch(int sequence, ProviderDispatcher dispatcher, byte messageId, byte[] body) {
    if (defaultClient == null) {
      throw new IllegalArgumentException(
          "Provider must be initialized with a default client in order to use short dispatch"
              + " method");
    }
    dispatcher.dispatch(sequence, this, messageId, defaultClient, body);
  }
}
