package space.rymiel.shinkorobi.common.messenger.protocol;

import java.net.InetSocketAddress;

public class KorobiCommunication extends KorobiMessage {
  private final int sender;
  private final InetSocketAddress address;

  public KorobiCommunication(
      int sequence,
      int replyTo,
      int sender,
      String provider,
      byte messageId,
      byte[] body,
      InetSocketAddress sentBy) {
    super(sequence, replyTo, provider, messageId, body);
    this.sender = sender;
    this.address = sentBy;
  }

  public int sender() {
    return sender;
  }

  public InetSocketAddress address() {
    return this.address;
  }
}
