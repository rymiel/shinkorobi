package space.rymiel.shinkorobi.common;

import io.leangen.geantyref.TypeToken;
import org.spongepowered.configurate.CommentedConfigurationNode;
import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.configurate.loader.ConfigurationLoader;
import org.spongepowered.configurate.objectmapping.ObjectMapper;
import org.spongepowered.configurate.util.NamingSchemes;
import org.spongepowered.configurate.yaml.YamlConfigurationLoader;

import java.io.File;

public record ConfigurationManager<T>(File file, TypeToken<T> type, T instance) {
  public static final ObjectMapper.Factory objectFactory = ObjectMapper
      .factoryBuilder()
      .defaultNamingScheme(NamingSchemes.SNAKE_CASE)
      .build();
  private static final YamlConfigurationLoader.Builder configBuilder = YamlConfigurationLoader.builder()
      .defaultOptions(opts ->
          opts.serializers(builder -> builder.registerAnnotatedObjects(objectFactory))
              .shouldCopyDefaults(true)
      );

  public T loadConfig() throws ConfigurateException {
    var loader = saveDefault();
    return objectFactory.get(type).load(loader.load());
  }

  public ConfigurationLoader<CommentedConfigurationNode> saveDefault() throws ConfigurateException {
    var loader = configBuilder.path(file.toPath()).build();
    if (!file.exists()) {
      loader.save(loader.createNode().set(type, instance));
    }
    return loader;
  }
}
