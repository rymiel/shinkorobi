package space.rymiel.shinkorobi.common.provider;

import net.kyori.adventure.text.Component;
import space.rymiel.shinkorobi.common.provider.common.IMessageEnum;

public enum DebugMessage implements IMessageEnum {
  PING(0xf0),
  STARTUP(0x11),
  MANUAL(0x21),
  REGISTRY(0x31);

  private final byte id;

  DebugMessage(int id) {
    this.id = (byte) id;
  }

  public byte id() {
    return id;
  }

  public record RegistryResponse(Component[] lines) { }
}
