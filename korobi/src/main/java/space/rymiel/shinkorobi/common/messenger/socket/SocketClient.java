package space.rymiel.shinkorobi.common.messenger.socket;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import space.rymiel.shinkorobi.common.GlobalLog;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.common.messenger.MessengerProvider;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiCommunication;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiMessage;
import space.rymiel.shinkorobi.common.messenger.protocol.SocketFormat;
import space.rymiel.shinkorobi.common.messenger.registry.ProviderRegistry;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class SocketClient {
  protected final @NotNull AtomicBoolean isTransmitting;
  private String ip;
  private int port;
  private final LoggingPlugin pluginLog;
  private final File secretKeyPath;
  private final LinkedBlockingQueue<KorobiMessage> queue = new LinkedBlockingQueue<>();
  protected boolean isClosed = false;
  protected boolean isConnectionActive = false;
  private Socket socket;
  protected SecretKey key;
  private Thread socketThread;

  protected SocketClient(String ip, int port, LoggingPlugin pluginLog, File secretKeyPath) {
    this.ip = ip;
    this.port = port;
    this.pluginLog = pluginLog;
    this.secretKeyPath = secretKeyPath;
    this.isTransmitting = new AtomicBoolean(false);
    pluginLog.info("Korobi messages will be sent to /%s:%s", ip, port);
    loadKey();
    asynchronously(this::runSocket);
  }

  private void loadKey() {
    if (this.isKeyExisting()) {
      try {
        File file = secretKeyPath;
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bytes = new byte[(int) file.length()];
        fileInputStream.read(bytes);
        fileInputStream.close();
        this.key = new SecretKeySpec(bytes, "AES");
      } catch (IOException iOException) {
        iOException.printStackTrace();
      }
    } else {
      pluginLog.fatal(
          "Can't find the socket client AES key! It should be put at %s", secretKeyPath);
    }
  }

  public void restart(@Nullable String newIp, @Nullable Integer newPort) {
    closeIfRunning();
    this.ip = Objects.requireNonNullElse(newIp, ip);
    this.port = Objects.requireNonNullElse(newPort, port);
    asynchronously(this::runSocket);
  }

  public void closeIfRunning() {
    if (socket != null && !socket.isClosed()) {
      socketThread.interrupt();
      try {
        if (socket != null)
          socket.close();
        this.isClosed = true;
        pluginLog.debug("Closing socket");
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private boolean isKeyExisting() {
    return secretKeyPath.exists();
  }

  protected abstract void asynchronously(Runnable action);

  @SuppressWarnings("SameParameterValue")
  protected abstract void later(Runnable action, long delay);

  protected abstract int hash();

  public void submit(KorobiMessage message) {
    queue.add(message);
    if (this.socket == null && !this.isClosed) {
      pluginLog.warn("Tried to send a message, but a socket didn't exist, opening one now");
      asynchronously(this::runSocket);
    }
  }

  synchronized private void runSocket() {
    if (this.socket != null) {
      throw new UnsupportedOperationException("Tried to open a client socket while one is already running");
    }
    this.isClosed = false;
    try (Socket socket = new Socket(this.ip, this.port)) {
      this.socket = socket;
      this.socketThread = Thread.currentThread();

      SocketFormat.writePlainMagic(socket.getOutputStream());
      pluginLog.info("Client connected: %s through %s on %s", socket.getRemoteSocketAddress(), socket.getLocalSocketAddress(), socketThread);
      Cipher cipher = Cipher.getInstance("AES/CFB8/NoPadding");
      byte[] iv = socket.getInputStream().readNBytes(16);
      cipher.init(Cipher.ENCRYPT_MODE, this.key, new IvParameterSpec(iv));

      DataOutputStream out =
          new DataOutputStream(new CipherOutputStream(socket.getOutputStream(), cipher));
      DataInputStream in =
          new DataInputStream(new CipherInputStream(socket.getInputStream(), cipher));

      SocketFormat.buildPacketHeader(out, hash());

      while (!isClosed) {
        ArrayList<KorobiMessage> sent = new ArrayList<>();
        KorobiMessage packet = queue.poll(15L, TimeUnit.MINUTES);
        if (packet == null) {
          out.writeInt(0x00);
          out.flush();
          continue;
        }
        do {
          GlobalLog.debug("<submit> <packet> %s", packet);
          packet.writeTo(out);
          sent.add(packet);
        } while ((packet = this.queue.poll()) != null);

        out.writeInt(0x00);
        out.flush();
        GlobalLog.debug("Sent %s messages", sent.size());

        for (KorobiMessage check : sent) {
          int incomingCheckHash = in.readInt();
          int actualCheckHash = check.packetCheckHash();
          GlobalLog.debug("  match: %s for %s", incomingCheckHash == actualCheckHash, check);
        }

        var messages = SocketFormat.readMessages(in);

        for (KorobiMessage msg : Objects.requireNonNull(messages)) {
          pluginLog.debug("Received: %s", msg.inspect());
          out.writeInt(msg.packetCheckHash());
          handleResponse(
              new KorobiCommunication(
                  msg.sequence(),
                  msg.replyTo(),
                  -1, // Server is always sender "-1"
                  msg.provider(),
                  msg.messageId(),
                  msg.body(),
                  (InetSocketAddress) socket.getRemoteSocketAddress()));
        }
      }
    } catch (InterruptedException e) {
      pluginLog.info("Gracefully shut down socket client");
    } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
             InvalidAlgorithmParameterException | SocketFormat.IncomingPacketFormatException e) {
      e.printStackTrace();
    } finally {
      this.socket = null;
    }
  }

  protected abstract void handleResponse(KorobiCommunication response);

  public static class Builder {
    public static <T extends MessengerProvider> @NotNull SocketClient build(
        @NotNull T provider, ProviderRegistry registry, String ip, int port, String filename) {
      return new SocketClient(
          ip, port, provider.plugin(), new File(provider.dataFolder(), filename)) {
        @Override
        protected void asynchronously(Runnable action) {
          provider.asynchronously(action);
        }

        @Override
        protected void later(Runnable action, long delayMs) {
          provider.later(action, delayMs);
        }

        @Override
        protected int hash() {
          return provider.hash();
        }

        @Override
        protected void handleResponse(KorobiCommunication response) {
          registry.consumeResponse(response);
        }
      };
    }
  }
}
