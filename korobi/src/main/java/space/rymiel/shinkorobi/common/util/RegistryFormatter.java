package space.rymiel.shinkorobi.common.util;

import net.kyori.adventure.text.Component;
import space.rymiel.shinkorobi.api.KorobiExtension;
import space.rymiel.shinkorobi.common.messenger.registry.ProviderRegistry;
import space.rymiel.shinkorobi.common.provider.common.AbstractProvider;

import java.util.List;
import java.util.Map;

import static net.kyori.adventure.text.Component.text;
import static net.kyori.adventure.text.format.NamedTextColor.*;
import static net.kyori.adventure.text.format.TextDecoration.BOLD;
import static net.kyori.adventure.text.format.TextDecoration.ITALIC;

public class RegistryFormatter {
  public static void formatRegistry(List<Component> format, Map<AbstractProvider, Map<Byte, ProviderRegistry.Entry>> reg) {
    reg.forEach(
        (k, v) -> {
          var providerName = k.getClass().getName();
          if (k instanceof KorobiExtension.ExtensionProvider extensionProvider) {
            providerName = "extension " + extensionProvider.registeringClass().getName();
          }
          format.add(
              Component.empty()
                  .append(text("  -- Provider ", GOLD))
                  .append(text(k.name, GOLD, BOLD))
                  .append(text("@", GRAY))
                  .append(text(providerName, YELLOW, ITALIC)));
          v.forEach(
              (b, e) -> {
                var messageByte = String.format("%02x", b);
                var receiverType = e.callback() == null ? "[declared]" : e.callback().toString();
                if (receiverType.startsWith(ProviderRegistry.Entry.Builder.class.getName())) {
                  receiverType = "[" + e.meta().toString().toLowerCase() + "]";
                }
                var lambdaSplit = receiverType.split("\\$\\$Lambda\\$");
                if (lambdaSplit.length > 1) {
                  receiverType = "lambda@" + lambdaSplit[0];
                }
                format.add(
                    Component.empty()
                        .append(text("    * ", DARK_PURPLE))
                        .append(text(messageByte, LIGHT_PURPLE, BOLD))
                        .append(text(" => ", GRAY))
                        .append(text(e.name(), LIGHT_PURPLE))
                        .append(Component.space())
                        .append(text(receiverType, GRAY, ITALIC)));
              });
        });
  }
}
