package space.rymiel.shinkorobi.common.provider;

import space.rymiel.shinkorobi.common.provider.common.IMessageEnum;

public enum BuiltinMessage implements IMessageEnum {
  HANDSHAKE(0x01),
  CONFIG_RELOAD(0x02);

  private final byte id;

  BuiltinMessage(int id) {
    this.id = (byte) id;
  }

  public byte id() {
    return id;
  }

  public record Handshake(String name, String ip, int port, String customName) {
  }
}
