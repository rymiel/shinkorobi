package space.rymiel.shinkorobi.common;

import space.rymiel.shinkorobi.common.messenger.MessengerProvider;
import space.rymiel.shinkorobi.common.messenger.registry.ProviderRegistry;

public interface RegistryPlugin extends LoggingPlugin {
  ProviderRegistry registry();

  MessengerProvider messengerProvider();
}
