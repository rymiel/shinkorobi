package space.rymiel.shinkorobi.common.util;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public class FileUtil {

  @NotNull
  public static <T> List<Class<? extends T>> findClasses(
      @NotNull final URL[] jars, @NotNull final Class<T> clazz)
      throws IOException, ClassNotFoundException {
    final URLClassLoader loader = new URLClassLoader(jars, clazz.getClassLoader());
    final List<Class<? extends T>> classes = new ArrayList<>();

    for (final URL jar : jars) {
      try (final JarInputStream stream = new JarInputStream(jar.openStream())) {
        final List<String> matches = new ArrayList<>();
        JarEntry entry;
        while ((entry = stream.getNextJarEntry()) != null) {
          final String name = entry.getName();
          if (!name.endsWith(".class"))
            continue;
          // GlobalLog.debug("Jar %s - %s (%s)", jar, name, entry);

          matches.add(name.substring(0, name.lastIndexOf('.')).replace('/', '.'));
        }

        for (final String match : matches) {
          try {
            final Class<?> loaded = loader.loadClass(match);
            if (clazz.isAssignableFrom(loaded)) {
              classes.add(loaded.asSubclass(clazz));
            }
          } catch (final NoClassDefFoundError | IllegalAccessError ignored) {
          }
        }
      }
      if (classes.isEmpty()) {
        loader.close();
        return List.of();
      }
    }
    return classes;
  }

  public static <T> T getInstance(@NotNull Class<? extends T> cls) {
    try {
      return cls.getDeclaredConstructor().newInstance();
    } catch (InstantiationException
             | IllegalAccessException
             | InvocationTargetException
             | NoSuchMethodException e) {
      e.printStackTrace();
      return null;
    }
  }
}
