package space.rymiel.shinkorobi.common.messenger;

import org.jetbrains.annotations.NotNull;
import space.rymiel.shinkorobi.common.LoggingPlugin;

import java.io.File;

public abstract class MessengerProvider {
  public abstract void asynchronously(Runnable action);

  public abstract void later(Runnable action, long delayMs);

  public abstract LoggingPlugin plugin();

  public abstract @NotNull File dataFolder();

  public abstract int hash();
}
