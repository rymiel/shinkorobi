package space.rymiel.shinkorobi.common.util;

import org.jetbrains.annotations.NotNull;

public class BytePayloadUtil {
  private BytePayloadUtil() {
  }

  public static @NotNull String formatColor(byte @NotNull [] bytes) {
    StringBuilder sb = new StringBuilder(bytes.length * 3 + 2);
    for (byte aByte : bytes) {
      if (aByte < 0) {
        sb.append("§5-");
      } else if (aByte >= 127) {
        sb.append("§d@");
      } else if (aByte < 10) {
        sb.append("§2").append(aByte);
      } else if (aByte < 32) {
        sb.append("§c@");
      } else {
        sb.append("§b");
        sb.append((char) aByte);
      }
    }
    sb.append("§r");
    return sb.toString();
  }

  public static @NotNull String format(byte @NotNull [] bytes) {
    StringBuilder sb = new StringBuilder(bytes.length);
    for (byte aByte : bytes) {
      if (aByte >= 32 && aByte < 127) {
        sb.append((char) aByte);
      } else {
        sb.append('.');
      }
    }
    return sb.toString();
  }
}
