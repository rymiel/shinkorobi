package space.rymiel.shinkorobi.common.messenger.registry;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import space.rymiel.shinkorobi.api.AMessage;
import space.rymiel.shinkorobi.api.MessageListener;
import space.rymiel.shinkorobi.common.messenger.dispatcher.ProviderDispatcher;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiCommunication;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiMessage;
import space.rymiel.shinkorobi.common.provider.common.AbstractProvider;
import space.rymiel.shinkorobi.common.provider.common.IMessageEnum;
import space.rymiel.shinkorobi.common.serializer.RecordSerializer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ProviderRegistry {
  private final Map<String, AbstractProvider> providerMap = new HashMap<>();
  private final Map<AbstractProvider, Map<Byte, Entry>> registryMap = new HashMap<>();
  private final Map<Integer, String> serverHashesMap;
  private final Map<Integer, Consumer<KorobiCommunication>> responseMap = new HashMap<>();
  private final ProviderDispatcher dispatcher = new ProviderDispatcher(this);
  private final AtomicInteger sequence = new AtomicInteger(1);

  public ProviderRegistry() {
    this.serverHashesMap = new HashMap<>();
  }

  public ProviderRegistry(Map<Integer, String> backingServerHashesMap) {
    this.serverHashesMap = backingServerHashesMap;
  }

  public @NotNull Map<AbstractProvider, Map<Byte, Entry>> getRegistryMap() {
    return registryMap;
  }

  public void addProvider(@NotNull AbstractProvider provider) {
    if (providerMap.get(provider.name) != null) {
      throw new IllegalArgumentException(
          "Provider with name \"" + provider.name + "\" already exists.");
    }
    providerMap.put(provider.name, provider);
    registryMap.put(provider, new HashMap<>());
  }

  public void addListenerToProvider(
      @NotNull AbstractProvider provider,
      byte messageId,
      String name,
      BiConsumer<KorobiCommunication, ByteArrayDataOutput> callback) {
    registryMap.get(provider).put(messageId, new Entry(name, callback));
  }

  public void addListenerToProvider(
      @NotNull AbstractProvider provider,
      byte messageId,
      String name,
      BiConsumer<KorobiCommunication, ByteArrayDataOutput> callback,
      Entry.Meta meta) {
    registryMap.get(provider).put(messageId, new Entry(name, callback, meta));
  }

  public void addNameToServerHash(int hash, String name) {
    serverHashesMap.put(hash, name);
  }

  public String resolveServerHash(int hash) {
    return serverHashesMap.getOrDefault(hash, String.format("0x%08X", hash));
  }

  public @Nullable String checkMessageRegistration(
      AbstractProvider provider, byte messageId, boolean hasCallback) {
    var messages = registryMap.get(provider);
    if (messages == null) {
      return null;
    }
    var messageEntry = messages.get(messageId);
    if (messageEntry == null) {
      return null;
    }
    if ((messageEntry.callback != null) == hasCallback) {
      return messageEntry.name;
    } else {
      return null;
    }
  }

  public @NotNull ProviderRegistry.Entry.Builder build(@NotNull AbstractProvider provider) {
    addProvider(provider);
    return new Entry.Builder(this, provider);
  }

  public AbstractProvider provider(String name) {
    return providerMap.get(name);
  }

  public AbstractProvider provider(Class<? extends IMessageEnum> messages) {
    return providerMap.values().stream()
        .filter(abstractProvider -> messages.equals(abstractProvider.messages))
        .findFirst()
        .orElseThrow();
  }

  public Entry entry(AbstractProvider provider, byte messageId) {
    return registryMap.get(provider).get(messageId);
  }

  public ProviderDispatcher getDispatcher() {
    return dispatcher;
  }

  public void consumeResponse(KorobiCommunication response) {
    int replyTo = response.replyTo();
    var consumer = responseMap.remove(replyTo);
    if (consumer == null) {
      // GlobalLog.warn("Message with sequence %s didn't expect a response and %s cannot be routed to it", response.replyTo(), response);
      return; // discard
    }
    consumer.accept(response);
  }

  public void dispatch(IMessageEnum message, byte[] body) {
    var provider = provider(message.getClass());
    provider.dispatch(sequence.getAndIncrement(), dispatcher, message.id(), body);
  }

  public void dispatch(IMessageEnum message, byte[] body, Consumer<KorobiCommunication> response) {
    var provider = provider(message.getClass());
    responseMap.put(sequence.get(), response);
    provider.dispatch(sequence.getAndIncrement(), dispatcher, message.id(), body);
  }

  public void dispatch(IMessageEnum message, Consumer<ByteArrayDataOutput> callback) {
    var out = ByteStreams.newDataOutput();
    callback.accept(out);
    dispatch(message, out.toByteArray());
  }

  public void dispatch(IMessageEnum message, Record record) {
    dispatch(message, RecordSerializer.serializeRecord(record));
  }

  @SuppressWarnings("unchecked")
  public <T extends Enum<T> & IMessageEnum> void dispatch(Record record) {
    if (record.getClass().isAnnotationPresent(AMessage.class)) {
      AMessage ann = record.getClass().getAnnotation(AMessage.class);
      IMessageEnum e = Enum.valueOf((Class<T>) ann.i(), ann.name());
      dispatch(e, record);
    } else {
      throw new IllegalStateException(
          "Record type must be annotated with @AMessage to be used directly");
    }
  }

  @SuppressWarnings("unchecked")
  public <T extends Enum<T> & IMessageEnum> void dispatch(Record record, Consumer<KorobiCommunication> response) {
    if (record.getClass().isAnnotationPresent(AMessage.class)) {
      AMessage ann = record.getClass().getAnnotation(AMessage.class);
      IMessageEnum e = Enum.valueOf((Class<T>) ann.i(), ann.name());
      dispatch(e, record, response);
    } else {
      throw new IllegalStateException(
          "Record type must be annotated with @AMessage to be used directly");
    }
  }

  public void dispatch(
      IMessageEnum message, Record record, Consumer<KorobiCommunication> response) {
    dispatch(message, RecordSerializer.serializeRecord(record), response);
  }

  public void dispatch(IMessageEnum message, String simpleBody) {
    dispatch(message, (out) -> out.writeUTF(simpleBody));
  }

  public record Entry(String name, BiConsumer<KorobiCommunication, ByteArrayDataOutput> callback, Meta meta) {
    public enum Meta {
      CONSUMED, EXTENSION, CALLBACK, UNKNOWN
    }

    public Entry(String name, BiConsumer<KorobiCommunication, ByteArrayDataOutput> callback) {
      this(name, callback, Meta.UNKNOWN);
    }

    public KorobiMessage execute(KorobiCommunication message, ProviderRegistry registry) {
      if (callback != null) {
        var responseOut = ByteStreams.newDataOutput();
        callback.accept(message, responseOut);
        var responseBody = responseOut.toByteArray();
        return new KorobiMessage(
            registry.sequence.getAndIncrement(),
            message.sequence(),
            message.provider(),
            message.messageId(),
            responseBody);
      }
      return null;
    }

    @SuppressWarnings("UnusedReturnValue")
    public record Builder(ProviderRegistry registry, AbstractProvider provider) {

      public @NotNull ProviderRegistry.Entry.Builder listen(
          byte messageId, String name, Consumer<KorobiCommunication> callback) {
        registry.addListenerToProvider(
            provider, messageId, name, (c, unused) -> callback.accept(c), Meta.CONSUMED);
        return this;
      }

      public @NotNull ProviderRegistry.Entry.Builder listen(
          byte messageId, String name, Consumer<KorobiCommunication> callback, Meta meta) {
        registry.addListenerToProvider(
            provider, messageId, name, (c, unused) -> callback.accept(c), meta);
        return this;
      }

      public @NotNull ProviderRegistry.Entry.Builder listen(
          byte messageId,
          String name,
          BiConsumer<KorobiCommunication, ByteArrayDataOutput> callback) {
        registry.addListenerToProvider(provider, messageId, name, callback, Meta.CALLBACK);
        return this;
      }

      public @NotNull ProviderRegistry.Entry.Builder listen(
          byte messageId,
          String name,
          BiConsumer<KorobiCommunication, ByteArrayDataOutput> callback, Meta meta) {
        registry.addListenerToProvider(provider, messageId, name, callback, meta);
        return this;
      }

      public @NotNull ProviderRegistry.Entry.Builder listen(
          @NotNull IMessageEnum e, Consumer<KorobiCommunication> callback) {
        return listen(e.id(), e.name(), callback);
      }

      public @NotNull ProviderRegistry.Entry.Builder listen(
          @NotNull IMessageEnum e, Consumer<KorobiCommunication> callback, Meta meta) {
        return listen(e.id(), e.name(), callback, meta);
      }

      public @NotNull ProviderRegistry.Entry.Builder listen(
          @NotNull IMessageEnum e, BiConsumer<KorobiCommunication, ByteArrayDataOutput> callback) {
        return listen(e.id(), e.name(), callback);
      }

      public @NotNull ProviderRegistry.Entry.Builder listen(
          @NotNull IMessageEnum e, BiConsumer<KorobiCommunication, ByteArrayDataOutput> callback, Meta meta) {
        return listen(e.id(), e.name(), callback, meta);
      }

      @SuppressWarnings("unchecked")
      public <T, E extends Enum<E> & IMessageEnum> ProviderRegistry.Entry.Builder listenAll(
          T extension) {
        Set<Method> methods = new HashSet<>();
        Collections.addAll(methods, extension.getClass().getMethods());
        Collections.addAll(methods, extension.getClass().getDeclaredMethods());
        for (final Method method : methods) {
          if (!method.isAnnotationPresent(MessageListener.class)) continue;
          if (method.isBridge() || method.isSynthetic()) continue;

          var params = method.getParameterTypes();
          if (params.length == 1 || params.length == 2 || params.length == 3) {
            var targetRec = params[0];
            if (!targetRec.isRecord()) {
              throw new IllegalStateException(
                  "Message handler "
                      + method.toGenericString()
                      + " tried to listen to non-record message");
            }
            if (!targetRec.isAnnotationPresent(AMessage.class)) {
              throw new IllegalStateException(
                  "Message handler "
                      + method.toGenericString()
                      + " tried to listen to message with no @AMessage annotation");
            }
            if (params.length == 2 && !KorobiCommunication.class.isAssignableFrom(params[1])) {
              throw new IllegalStateException(
                  "Second argument of message handler "
                      + method.toGenericString()
                      + " must be KorobiCommunication, or not present at all");
            }
            if (params.length == 3 && !ByteArrayDataOutput.class.isAssignableFrom(params[2])) {
              throw new IllegalStateException(
                  "Third argument of message handler "
                      + method.toGenericString()
                      + " must be ByteArrayDataOutput, or not present at all");
            }
            AMessage ann = targetRec.getAnnotation(AMessage.class);
            IMessageEnum e = Enum.valueOf((Class<E>) ann.i(), ann.name());
            listen(
                e,
                ((com, res) -> {
                  var rec =
                      RecordSerializer.deserializeRecord(
                          ByteStreams.newDataInput(com.body()), (Class<Record>) targetRec);
                  try {
                    if (params.length == 2) method.invoke(extension, rec, com);
                    else if (params.length == 3) method.invoke(extension, rec, com, res);
                    else method.invoke(extension, rec);
                  } catch (IllegalAccessException
                      | InvocationTargetException
                      | IllegalArgumentException ex) {
                    throw new IllegalStateException(
                        "Message handler "
                            + method.toGenericString()
                            + " didn't accept promised type "
                            + rec,
                        ex);
                  }
                }), Meta.EXTENSION);
          } else {
            throw new IllegalStateException(
                "Message handler " + method.toGenericString() + " has an invalid signature");
          }
        }
        return this;
      }

      public @NotNull ProviderRegistry.Entry.Builder consume(byte messageId, String name) {
        registry.addListenerToProvider(provider, messageId, name, (e, f) -> {
        }, Meta.CONSUMED);
        return this;
      }

      public @NotNull ProviderRegistry.Entry.Builder consume(@NotNull IMessageEnum e) {
        return consume(e.id(), e.name());
      }

      public @NotNull ProviderRegistry.Entry.Builder declare(byte messageId, String name) {
        registry.addListenerToProvider(provider, messageId, name, null);
        return this;
      }

      public @NotNull <E extends IMessageEnum> ProviderRegistry.Entry.Builder declareAll(
          Class<E> e) {
        var enumEntries = e.getEnumConstants();
        for (E enumEntry : enumEntries) {
          declare(enumEntry.id(), enumEntry.name());
        }
        return this;
      }
    }
  }
}
