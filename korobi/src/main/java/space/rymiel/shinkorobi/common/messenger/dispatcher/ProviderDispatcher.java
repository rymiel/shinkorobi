package space.rymiel.shinkorobi.common.messenger.dispatcher;

import org.jetbrains.annotations.NotNull;
import space.rymiel.shinkorobi.common.GlobalLog;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiCommunication;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiMessage;
import space.rymiel.shinkorobi.common.messenger.registry.ProviderRegistry;
import space.rymiel.shinkorobi.common.messenger.socket.SocketClient;
import space.rymiel.shinkorobi.common.provider.common.AbstractProvider;

public record ProviderDispatcher(ProviderRegistry registry) {

  public void dispatch(
      int sequence,
      AbstractProvider provider,
      byte messageId,
      @NotNull SocketClient client,
      byte[] body) {
    var messageName = registry.checkMessageRegistration(provider, messageId, false);
    if (messageName == null) {
      throw new IllegalArgumentException(
          String.format(
              "Message 0x%02x owned by %s doesn't exist or is registered incorrectly.",
              messageId, provider.name));
    }
    GlobalLog.debug("Dispatching %s / %s (0x%02x)", provider.name, messageName, messageId);
    client.submit(new KorobiMessage(sequence, provider.name, messageId, body));
  }

  public KorobiMessage execute(KorobiCommunication incoming) {
    GlobalLog.debug(
        "[%s] Incoming %s / 0x%02x",
        registry.resolveServerHash(incoming.sender()), incoming.provider(), incoming.messageId());
    var owningProvider = registry.provider(incoming.provider());
    if (owningProvider == null) {
      throw new IllegalArgumentException(
          String.format(
              "Provider %s doesn't exist and incoming message 0x%02x cannot be routed to it.",
              incoming.provider(), incoming.messageId()));
    }
    var messageName = registry.checkMessageRegistration(owningProvider, incoming.messageId(), true);
    if (messageName == null) {
      throw new IllegalArgumentException(
          String.format(
              "Message 0x%02x owned by %s doesn't exist or is registered incorrectly.",
              incoming.messageId(), owningProvider.name));
    }
    GlobalLog.debug(
        "[%s] Executing %s / %s (0x%02x)",
        registry.resolveServerHash(incoming.sender()),
        incoming.provider(),
        messageName,
        incoming.messageId());
    return registry.entry(owningProvider, incoming.messageId()).execute(incoming, registry);
  }
}
