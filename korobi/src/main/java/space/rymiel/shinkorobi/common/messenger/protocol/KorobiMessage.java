package space.rymiel.shinkorobi.common.messenger.protocol;

import space.rymiel.shinkorobi.common.util.BytePayloadUtil;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class KorobiMessage {
  private final int sequence;
  private final int replyTo;
  private final String provider;
  private final byte messageId;
  private final byte[] body;

  public KorobiMessage(int sequence, String provider, byte messageId, byte[] body) {
    this.sequence = sequence;
    this.replyTo = 0;
    this.provider = provider;
    this.messageId = messageId;
    this.body = body;
  }

  public KorobiMessage(int sequence, int replyTo, String provider, byte messageId, byte[] body) {
    this.sequence = sequence;
    this.replyTo = replyTo;
    this.provider = provider;
    this.messageId = messageId;
    this.body = body;
  }

  public int sequence() {
    return sequence;
  }

  public int replyTo() {
    return replyTo;
  }

  public int packetCheckHash() {
    return Arrays.hashCode(body);
  }

  public byte[] body() {
    return body;
  }

  public byte messageId() {
    return messageId;
  }

  public String provider() {
    return provider;
  }

  public KorobiCommunication upgrade(KorobiHeader header) {
    return new KorobiCommunication(
        sequence,
        replyTo,
        header.sender(),
        provider,
        messageId,
        body,
        header.source());
  }

  public void writeTo(DataOutputStream out) throws IOException {
    out.writeInt(sequence);
    out.writeInt(replyTo);
    out.writeUTF(provider);
    out.writeByte(messageId);
    out.writeInt(0); // reserved for provider version
    out.writeInt(body.length);
    out.write(body);
  }

  @Override
  public String toString() {
    return String.format(
        "KorobiMessage[%s->%s]{%s::%02x, %s}",
        sequence, replyTo, provider, messageId, BytePayloadUtil.format(body));
  }

  public String inspect() {
    return String.format(
        "KorobiMessage[%s->%s]{%s::%02x, %s}",
        sequence, replyTo, provider, messageId, BytePayloadUtil.formatColor(body));
  }
}
