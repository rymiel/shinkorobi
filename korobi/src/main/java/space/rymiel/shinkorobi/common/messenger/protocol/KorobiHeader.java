package space.rymiel.shinkorobi.common.messenger.protocol;

import java.net.InetSocketAddress;

public record KorobiHeader(int version, int sender, InetSocketAddress source) {
}
