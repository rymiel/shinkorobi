package space.rymiel.shinkorobi.common;

import org.jetbrains.annotations.NotNull;

import java.util.logging.Logger;

public class GlobalLog {
  private static LoggingPlugin globalLog = new NullLogger();

  public static void setLogger(LoggingPlugin log) {
    globalLog = log;
  }

  public static void info(@NotNull Object msg, Object... ext) {
    globalLog.info(msg, ext);
  }

  public static void debug(@NotNull Object msg, Object... ext) {
    globalLog.debug(msg, ext);
  }

  public static void fatal(@NotNull Object msg, Object... ext) {
    globalLog.fatal(msg, ext);
  }

  public static void warn(@NotNull Object msg, Object... ext) {
    globalLog.warn(msg, ext);
  }

  private static class NullLogger implements LoggingPlugin {

    @Override
    public boolean getDebugMode() {
      return true;
    }

    @Override
    public void info(@NotNull Object msg, Object... ext) {
      Logger.getLogger("NullLogger").info(String.format(msg.toString(), ext));
    }

    @Override
    public void debug(@NotNull Object msg, Object... ext) {
      Logger.getLogger("NullLogger").info("[DEBUG] " + String.format(msg.toString(), ext));
    }

    @Override
    public void fatal(@NotNull Object msg, Object... ext) {
      Logger.getLogger("NullLogger").severe(String.format(msg.toString(), ext));
    }

    @Override
    public void warn(@NotNull Object msg, Object... ext) {
      Logger.getLogger("NullLogger").warning(String.format(msg.toString(), ext));
    }
  }
}
