package space.rymiel.shinkorobi.common.messenger.socket;

import com.google.common.io.BaseEncoding;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import space.rymiel.shinkorobi.common.GlobalLog;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.common.messenger.MessengerProvider;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiCommunication;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiHeader;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiMessage;
import space.rymiel.shinkorobi.common.messenger.protocol.SocketFormat;
import space.rymiel.shinkorobi.common.messenger.registry.ProviderRegistry;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;

public abstract class SocketServer {
  protected final LoggingPlugin pluginLog;
  protected final File secretKeyPath;
  protected ServerSocket serverSocket;
  protected @Nullable SecretKey key;

  protected SocketServer(String bindTo, int port, LoggingPlugin pluginLog, File secretKeyPath) {
    this.pluginLog = pluginLog;
    this.secretKeyPath = secretKeyPath;
    this.loadKey();
    try {
      this.serverSocket = new ServerSocket(port, 20, InetAddress.getByName(bindTo));
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    this.startListening();
  }

  private void loadKey() {
    if (this.isKeyExisting()) {
      try {
        File file = secretKeyPath;
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bytes = new byte[(int) file.length()];
        fileInputStream.read(bytes);
        fileInputStream.close();
        this.key = new SecretKeySpec(bytes, "AES");
      } catch (IOException iOException) {
        iOException.printStackTrace();
      }
    } else {
      this.key = this.generateKey();
    }
  }

  public void closeIfRunning() {
    if (serverSocket != null && !serverSocket.isClosed()) {
      try {
        serverSocket.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private boolean isKeyExisting() {
    return secretKeyPath.exists();
  }

  private @Nullable SecretKey generateKey() {
    try {
      KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
      keyGenerator.init(256);
      SecretKey secretKey = keyGenerator.generateKey();
      byte[] bytes = secretKey.getEncoded();
      pluginLog.debug("New key b16 %s", BaseEncoding.base16().encode(bytes));
      pluginLog.debug("New key b64 %s", BaseEncoding.base64().encode(bytes));
      FileOutputStream fileOutputStream = new FileOutputStream(secretKeyPath);
      fileOutputStream.write(bytes);
      fileOutputStream.close();
      return secretKey;
    } catch (IOException | NoSuchAlgorithmException exception) {
      exception.printStackTrace();
      return null;
    }
  }

  protected abstract void asynchronously(Runnable action);

  protected abstract @Nullable KorobiMessage handleIncoming(KorobiCommunication packet);

  private void handleSocket(Socket socket, KorobiHeader header, DataInputStream in, DataOutputStream out) {
    try {

      while (!socket.isClosed()) {
        var messages = SocketFormat.readMessages(in);
        if (messages == null) {
          socket.close();
          pluginLog.debug("A client has closed the connection: %s", header.sender());
          return;
        }
        if (messages.size() == 0) continue;
        pluginLog.debug("Recovered %s messages", messages.size());

        var responses = new HashMap<Integer, KorobiMessage>();
        for (KorobiMessage msg : messages) {
          pluginLog.debug("Received: %s", msg.inspect());
          out.writeInt(msg.packetCheckHash());
          var response = handleIncoming(msg.upgrade(header));
          if (response != null) {
            responses.put(msg.sequence(), response);
          }
        }

        for (var r : responses.entrySet()) {
          var packet = r.getValue();
          packet.writeTo(out);
        }
        out.writeInt(0x00);

        for (KorobiMessage check : responses.values()) {
          int incomingCheckHash = in.readInt();
          int actualCheckHash = check.packetCheckHash();
          GlobalLog.debug("  match: %s for %s", incomingCheckHash == actualCheckHash, check);
        }
      }
    } catch (IOException | SocketFormat.IncomingPacketFormatException e) {
      e.printStackTrace();
    }
  }

  private void buildConnection(Socket socket) {
    var address = (InetSocketAddress) socket.getRemoteSocketAddress();

    try {
      socket.setSoTimeout(10000);
      var error = SocketFormat.readPlainMagic(new DataInputStream(socket.getInputStream()));
      if (error != null) {
        pluginLog.warn("%s tried to create a connection that doesn't look like a ShinKorobi connection: %s", address, error);
        socket.close();
        return;
      }
      socket.setSoTimeout(0);

      Cipher cipher = Cipher.getInstance("AES/CFB8/NoPadding");
      SecureRandom random = new SecureRandom();
      byte[] iv = new byte[16];
      random.nextBytes(iv);
      cipher.init(Cipher.DECRYPT_MODE, this.key, new IvParameterSpec(iv));
      socket.getOutputStream().write(iv);
      socket.getOutputStream().flush();

      DataInputStream in =
          new DataInputStream(new CipherInputStream(socket.getInputStream(), cipher));
      DataOutputStream out =
          new DataOutputStream(new CipherOutputStream(socket.getOutputStream(), cipher));

      KorobiHeader header = SocketFormat.readHeader(in, address);

      asynchronously(() -> handleSocket(socket, header, in, out));
    } catch (SocketFormat.IncomingHeaderUnrelatedException e) {
      pluginLog.warn("%s tried to create a connection that failed the encryption check: %s", address, e.getMessage());
    } catch (SocketTimeoutException e) {
      pluginLog.warn("%s tried to create a connection without ever sending any data", address);
    } catch (SocketException e) {
      if (e.getMessage().contains("Socket closed")) {
        pluginLog.info("Server socket shut down gracefully");
        return; // discard
      }
      e.printStackTrace();
    } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
             InvalidAlgorithmParameterException | SocketFormat.IncomingPacketFormatException e) {
      e.printStackTrace();
    }
  }

  protected void startListening() {
    if (!this.isKeyExisting()) {
      return;
    }
    asynchronously(
        () -> {
          pluginLog.info(
              "Start Listening %s port %s (persistent)",
              serverSocket.getInetAddress(), serverSocket.getLocalPort());
          while (!this.serverSocket.isClosed()) {
            try {
              final Socket socket = this.serverSocket.accept();
              pluginLog.debug("Incoming transmission");
              asynchronously(() -> buildConnection(socket));
            } catch (SocketException e) {
              if (e.getMessage().contains("Socket closed")) {
                pluginLog.info("Server socket shut down gracefully");
                return; // discard
              }
              e.printStackTrace();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        });
  }

  public static class Builder {
    public static <T extends MessengerProvider> @NotNull SocketServer build(
        @NotNull T provider, ProviderRegistry registry, String bindTo, int port, String filename) {
      return new SocketServer(
          bindTo, port, provider.plugin(), new File(provider.dataFolder(), filename)) {
        @Override
        protected void asynchronously(Runnable action) {
          provider.asynchronously(action);
        }

        @Override
        protected @Nullable KorobiMessage handleIncoming(KorobiCommunication packet) {
          return registry.getDispatcher().execute(packet);
        }
      };
    }
  }
}