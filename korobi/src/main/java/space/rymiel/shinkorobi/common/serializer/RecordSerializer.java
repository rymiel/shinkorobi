package space.rymiel.shinkorobi.common.serializer;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.UUID;

public class RecordSerializer {
  public static byte[] serializeRecord(Record record) {
    var out = ByteStreams.newDataOutput();
    try {
      for (var field : record.getClass().getDeclaredFields()) {
        var value = record.getClass().getDeclaredMethod(field.getName()).invoke(record);
        writeField(value, out, record);
      }
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      throw new IllegalStateException(String.format("Cannot access record %s", record), e);
    }
    return out.toByteArray();
  }

  public static void writeField(Object value, ByteArrayDataOutput out, Record record) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    if (value.getClass().isArray()) {
      int length = Array.getLength(value);
      out.writeInt(length);
      for (int i = 0; i < length; i++) {
        Object arrayElement = Array.get(value, i);
        writeField(arrayElement, out, record);
      }
    } else if (value.getClass().isRecord()) {
      for (var nestedField : record.getClass().getDeclaredFields()) {
        var nestedValue = record.getClass().getDeclaredMethod(nestedField.getName()).invoke(record);
        writeField(nestedValue, out, record);
      }
    } else if (value instanceof String string) {
      out.writeUTF(string);
    } else if (value instanceof Integer integer) {
      out.writeInt(integer);
    } else if (value instanceof Long lng) {
      out.writeLong(lng);
    } else if (value instanceof Double dbl) {
      out.writeDouble(dbl);
    } else if (value instanceof UUID uuid) {
      out.writeLong(uuid.getLeastSignificantBits());
      out.writeLong(uuid.getMostSignificantBits());
    } else if (value instanceof Boolean bool) {
      out.writeBoolean(bool);
    } else if (value instanceof Component component) {
      out.writeUTF(GsonComponentSerializer.gson().serialize(component));
    } else {
      throw new IllegalStateException("Unknown type of field %s (%s) on record %s".formatted(value.getClass(), value, record));
    }
  }

  @SuppressWarnings("unchecked")
  public static <T> T readField(
      Class<T> target, ByteArrayDataInput in, Class<? extends Record> record) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
    if (target.isArray()) {
      int length = in.readInt();
      var array = Array.newInstance(target.getComponentType(), length);
      for (int i = 0; i < length; i++) {
        Array.set(array, i, readField(target.getComponentType(), in, record));
      }
      return (T) array;
    } else if (target.isRecord()) {
      var ctorTypes = new ArrayList<Class<?>>();
      var ctorValues = new ArrayList<>();
      for (var nestedField : target.getDeclaredFields()) {
        ctorTypes.add(nestedField.getType());
        ctorValues.add(readField(nestedField.getType(), in, (Class<? extends Record>) target));
      }
      return target
          .getConstructor(ctorTypes.toArray(Class[]::new))
          .newInstance(ctorValues.toArray());
    } else if (target == String.class) {
      return (T) in.readUTF();
    } else if (target == int.class || target == Integer.class) {
      return (T) (Integer) in.readInt();
    } else if (target == long.class || target == Long.class) {
      return (T) (Long) in.readLong();
    } else if (target == double.class || target == Double.class) {
      return (T) (Double) in.readDouble();
    } else if (target == UUID.class) {
      long lsb = in.readLong();
      long msb = in.readLong();
      return (T) new UUID(msb, lsb);
    } else if (target == Component.class) {
      String json = in.readUTF();
      return (T) GsonComponentSerializer.gson().deserialize(json);
    } else if (target == boolean.class || target == Boolean.class) {
      return (T) (Boolean) in.readBoolean();
    } else {
      throw new IllegalStateException(
          String.format("Unknown type of field %s on record type %s", target, record));
    }
  }

  public static <R extends Record> R deserializeRecord(ByteArrayDataInput in, Class<R> target) {
    var parameters = new ArrayList<>();
    var parameterTypes = new ArrayList<Class<?>>();
    try {
      for (var field : target.getDeclaredFields()) {
        parameterTypes.add(field.getType());
        parameters.add(readField(field.getType(), in, target));
      }
      return target
          .getConstructor(parameterTypes.toArray(Class[]::new))
          .newInstance(parameters.toArray());
    } catch (NoSuchMethodException e) {
      throw new IllegalStateException(
          String.format("Cannot construct record %s with types %s", target, parameterTypes), e);
    } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
      throw new IllegalStateException(
          String.format("Cannot construct record %s with %s", target, parameters), e);
    } catch (IllegalStateException e) {
      throw new IllegalStateException(String.format("Failed to construct record %s", target), e);
    }
  }
}
