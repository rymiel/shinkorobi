package space.rymiel.shinkorobi.common.messenger.protocol;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import space.rymiel.shinkorobi.common.util.BytePayloadUtil;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SocketFormat {
  private static final int VERSION = Integer.parseInt("@protocol_version@");
  private static final byte[] ENCRYPTED_MAGIC = {0x17, 0x4b, 0x6f, 0x72, 0x6f, 0x62, 0x69, 0x00};
  private static final byte[] PLAIN_MAGIC = {0x71, 0x53, 0x68, 0x69, 0x6e, 0x4b, 0x6f, 0x72, 0x6f, 0x62, 0x69, 0x00};

  private SocketFormat() {
  }

  public static void writePlainMagic(@NotNull OutputStream stream) throws IOException {
    stream.write(PLAIN_MAGIC);
  }

  public static void buildPacketHeader(@NotNull DataOutputStream stream, int hash)
      throws IOException {
    stream.write(ENCRYPTED_MAGIC);
    stream.writeInt(VERSION);
    stream.writeInt(hash);
  }

  public static @Nullable String readPlainMagic(@NotNull DataInputStream in) {
    try {
      byte[] potentialMagic = new byte[PLAIN_MAGIC.length];
      // A Korobi client ought to send the whole magic at once right away, anything less trickling in is suspicious
      int actualRead = in.read(potentialMagic);
      if (actualRead != PLAIN_MAGIC.length)
        return "Incomplete magic: %s only supplied %s bytes, but need at least %s".formatted(BytePayloadUtil.format(potentialMagic), actualRead, PLAIN_MAGIC.length);

      if (!Arrays.equals(potentialMagic, PLAIN_MAGIC))
        return "Invalid magic: %s".formatted(BytePayloadUtil.format(potentialMagic));

      return null;
    } catch (IOException e) {
      return e.getMessage();
    }
  }

  public static @NotNull KorobiHeader readHeader(@NotNull DataInputStream in, InetSocketAddress source) throws IncomingPacketFormatException, IncomingHeaderUnrelatedException {
    try {
      byte[] potentialMagic = new byte[ENCRYPTED_MAGIC.length];
      // A Korobi client ought to send the whole magic at once right away, anything less trickling in is suspicious
      int actualRead = in.read(potentialMagic);
      if (actualRead != ENCRYPTED_MAGIC.length) {
        throw new IncomingHeaderUnrelatedException("Incomplete magic: %s only supplied %s bytes, but need at least %s".formatted(BytePayloadUtil.format(potentialMagic), actualRead, ENCRYPTED_MAGIC.length));
      }

      if (Arrays.equals(potentialMagic, ENCRYPTED_MAGIC)) {
        int incomingVersion = in.readInt();
        if (incomingVersion != VERSION) {
          throw new IncomingPacketFormatException(
              String.format("Incompatible versions, expected %s but got %s", VERSION, incomingVersion));
        }
        int sender = in.readInt();

        return new KorobiHeader(incomingVersion, sender, source);
      } else {
        throw new IncomingHeaderUnrelatedException(
            String.format("Invalid magic: %s", BytePayloadUtil.format(potentialMagic)));
      }
    } catch (IOException e) {
      throw new IncomingPacketFormatException(e);
    }
  }

  public static List<KorobiMessage> readMessages(DataInputStream in)
      throws IncomingPacketFormatException {
    var messages = new ArrayList<KorobiMessage>();
    try {
      while (true) {
        int sequenceID = in.readInt();
        if (sequenceID == 0) {
          break; // end of transmission
        }
        int replyTo = in.readInt();
        String provider = in.readUTF();
        byte messageId = in.readByte();
        /* int providerVersion = */
        in.readInt(); // unused as of now, but reserved
        int messageLength = in.readInt();
        byte[] body = in.readNBytes(messageLength);
        KorobiMessage msg = new KorobiMessage(sequenceID, replyTo, provider, messageId, body);
        messages.add(msg);
      }
      return messages;
    } catch (EOFException e) {
      return null;
    } catch (IOException e) {
      throw new IncomingPacketFormatException(e);
    }
  }

  public static class IncomingPacketFormatException extends Exception {

    public IncomingPacketFormatException(String message) {
      super(message);
    }

    public IncomingPacketFormatException(Throwable e) {
      super(e);
    }
  }

  public static class IncomingHeaderUnrelatedException extends Exception {
    public IncomingHeaderUnrelatedException(String message) {
      super(message);
    }
  }
}
