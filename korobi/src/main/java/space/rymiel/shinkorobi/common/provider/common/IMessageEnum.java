package space.rymiel.shinkorobi.common.provider.common;

import org.jetbrains.annotations.NotNull;

public interface IMessageEnum {
  int ordinal();

  default byte id() {
    return (byte) ordinal();
  }

  @NotNull
  String name();
}
