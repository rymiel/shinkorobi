package space.rymiel.shinkorobi.common;

import org.jetbrains.annotations.NotNull;

public interface LoggingPlugin {
  boolean getDebugMode();

  void info(@NotNull Object msg, Object... ext);

  void debug(@NotNull Object msg, Object... ext);

  void fatal(@NotNull Object msg, Object... ext);

  void warn(@NotNull Object msg, Object... ext);
}
