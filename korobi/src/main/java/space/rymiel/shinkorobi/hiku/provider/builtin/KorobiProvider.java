package space.rymiel.shinkorobi.hiku.provider.builtin;

import space.rymiel.shinkorobi.common.messenger.registry.ProviderRegistry;
import space.rymiel.shinkorobi.common.messenger.socket.SocketClient;
import space.rymiel.shinkorobi.common.provider.BuiltinMessage;
import space.rymiel.shinkorobi.common.provider.common.AbstractProvider;

public class KorobiProvider extends AbstractProvider {
  public KorobiProvider(SocketClient defaultClient) {
    super(defaultClient, BuiltinMessage.class, "korobi");
  }

  public void register(ProviderRegistry registry) {
    registry.build(this).declareAll(BuiltinMessage.class);
  }
}
