package space.rymiel.shinkorobi.hiku.command;

import cloud.commandframework.annotations.Argument;
import cloud.commandframework.annotations.CommandDescription;
import cloud.commandframework.annotations.CommandMethod;
import cloud.commandframework.annotations.CommandPermission;
import cloud.commandframework.annotations.specifier.Greedy;
import com.google.common.io.ByteStreams;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;
import space.rymiel.shinkorobi.api.KorobiExtension;
import space.rymiel.shinkorobi.common.provider.BuiltinMessage;
import space.rymiel.shinkorobi.common.provider.DebugMessage;
import space.rymiel.shinkorobi.common.util.RegistryFormatter;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;

import java.util.ArrayList;

import static net.kyori.adventure.text.Component.text;
import static net.kyori.adventure.text.format.NamedTextColor.AQUA;
import static net.kyori.adventure.text.format.TextDecoration.BOLD;

public record HikuKorobiCommands(ShinKorobiPaper plugin) {
  @CommandDescription("Send a manual message for debugging")
  @CommandMethod("korobi manual <msg>")
  @CommandPermission("korobi.cmd.manual")
  private void commandManual(
      final @NotNull CommandSender sender, final @Argument(value = "msg") @Greedy String message) {
    plugin.registry().dispatch(DebugMessage.MANUAL, message);
  }

  @CommandDescription("Send a barrage of manual messages for debugging")
  @CommandMethod("korobi barrage <count> <msg>")
  @CommandPermission("korobi.cmd.manual")
  private void commandBarrage(
      final @NotNull CommandSender sender,
      final @Argument(value = "count") int count,
      final @Argument(value = "msg") @Greedy String message) {
    for (int i = 0; i < count; i++) {
      plugin
          .registry()
          .dispatch(DebugMessage.MANUAL, String.format("Barrage %s/%s: %s", i + 1, count, message));
    }
  }

  @CommandDescription("Show the current Provider Registry for debugging")
  @CommandMethod("korobi registry")
  @CommandPermission("korobi.cmd.registry")
  private void commandRegistry(final @NotNull CommandSender sender) {
    var registryFormat = new ArrayList<Component>();
    registryFormat.add(text("=== Local registry ===", AQUA, BOLD));
    var map = plugin.registry().getRegistryMap();

    RegistryFormatter.formatRegistry(registryFormat, map);

    registryFormat.forEach(sender::sendMessage);

    plugin.registry().dispatch(
        DebugMessage.REGISTRY, new byte[0], (com) -> {
          var res = ByteStreams.newDataInput(com.body());
          var remoteFormat = new ArrayList<Component>();
          remoteFormat.add(text("=== Remote registry ===", AQUA, BOLD));

          var count = res.readInt();
          for (int i = 0; i < count; i++) {
            var component = GsonComponentSerializer.gson().deserialize(res.readUTF());
            remoteFormat.add(component);
          }

          remoteFormat.forEach(sender::sendMessage);
        }
    );
  }

  @CommandDescription("Reload config. Changing the secret key requires a restart.")
  @CommandMethod("korobi reload")
  @CommandPermission("korobi.cmd.reload")
  private void commandReload(final @NotNull CommandSender sender) {
    plugin.reloadConfig();
    Runnable restart = () -> plugin.messenger().restart(
        plugin.getConfig().getString("socket_connect_to_ip"),
        plugin.getConfig().getInt("socket_connect_to_port")
    );
    plugin.registry().dispatch(BuiltinMessage.CONFIG_RELOAD, new byte[0]);
    plugin.extensions().forEach(KorobiExtension::onReloadConfig);
    plugin.messengerProvider().later(restart, 1000);
    sender.sendMessage(
        "Instructed your current server and the proxy to reload configs. Note that changing the secret key still require a restart");
  }
}
