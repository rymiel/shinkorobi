package space.rymiel.shinkorobi.hiku;

import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.common.messenger.MessengerProvider;

import java.io.File;

public class PaperMessengerProvider extends MessengerProvider {
  private final ShinKorobiPaper plugin;

  public PaperMessengerProvider(ShinKorobiPaper plugin) {
    this.plugin = plugin;
  }

  @Override
  public void asynchronously(@NotNull Runnable action) {
    Bukkit.getScheduler().runTaskAsynchronously(plugin, action);
  }

  @Override
  public void later(@NotNull Runnable action, long delayMs) {
    Bukkit.getScheduler().runTaskLater(plugin, action, (long) (delayMs * 0.02));
  }

  @Override
  public LoggingPlugin plugin() {
    return plugin;
  }

  @Override
  public @NotNull File dataFolder() {
    return plugin.getDataFolder();
  }

  @Override
  public int hash() {
    return String.format("[%s]%s:%s", Bukkit.getName(), Bukkit.getIp(), Bukkit.getPort())
        .hashCode();
  }
}
