package space.rymiel.shinkorobi.hiku.provider.debug;

import space.rymiel.shinkorobi.common.messenger.registry.ProviderRegistry;
import space.rymiel.shinkorobi.common.messenger.socket.SocketClient;
import space.rymiel.shinkorobi.common.provider.DebugMessage;
import space.rymiel.shinkorobi.common.provider.common.AbstractProvider;

public class DebugProvider extends AbstractProvider {
  public DebugProvider(SocketClient defaultClient) {
    super(defaultClient, DebugMessage.class, "debug");
  }

  public void register(ProviderRegistry registry) {
    registry.build(this).declareAll(DebugMessage.class);
  }
}
