package space.rymiel.shinkorobi.hiku;

import cloud.commandframework.CommandTree;
import cloud.commandframework.annotations.AnnotationParser;
import cloud.commandframework.arguments.parser.ParserParameters;
import cloud.commandframework.arguments.parser.StandardParameters;
import cloud.commandframework.bukkit.CloudBukkitCapabilities;
import cloud.commandframework.execution.AsynchronousCommandExecutionCoordinator;
import cloud.commandframework.execution.CommandExecutionCoordinator;
import cloud.commandframework.meta.CommandMeta;
import cloud.commandframework.paper.PaperCommandManager;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import net.luckperms.api.LuckPermsProvider;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import space.rymiel.shinkorobi.common.GlobalLog;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.common.MessagingClient;
import space.rymiel.shinkorobi.common.RegistryPlugin;
import space.rymiel.shinkorobi.common.extensions.ExtensionRegistry;
import space.rymiel.shinkorobi.common.messenger.MessengerProvider;
import space.rymiel.shinkorobi.common.messenger.registry.ProviderRegistry;
import space.rymiel.shinkorobi.common.messenger.socket.SocketClient;
import space.rymiel.shinkorobi.hiku.command.HikuKorobiCommands;
import space.rymiel.shinkorobi.hiku.listener.LuckPermsListener;
import space.rymiel.shinkorobi.hiku.listener.StartupListener;
import space.rymiel.shinkorobi.hiku.provider.builtin.KorobiProvider;
import space.rymiel.shinkorobi.hiku.provider.debug.DebugProvider;

import java.util.function.Function;

public final class ShinKorobiPaper extends JavaPlugin
    implements LoggingPlugin, RegistryPlugin, MessagingClient {
  private static ShinKorobiPaper instance;
  private final ProviderRegistry registry = new ProviderRegistry();
  private final ExtensionRegistry extensions = new ExtensionRegistry();
  private SocketClient messenger;
  private PaperMessengerProvider messengerProvider;
  private PaperCommandManager<CommandSender> manager;

  private final LegacyComponentSerializer legacyLoggerSerializer = LegacyComponentSerializer.legacySection();

  public static ShinKorobiPaper getInstance() {
    return instance;
  }

  public SocketClient messenger() {
    return messenger;
  }

  public ProviderRegistry registry() {
    return registry;
  }

  public ExtensionRegistry extensions() {
    return extensions;
  }

  public PaperCommandManager<CommandSender> commandManager() {
    return manager;
  }

  @Override
  public MessengerProvider messengerProvider() {
    return messengerProvider;
  }

  @Override
  public void onEnable() {
    saveDefaultConfig();

    instance = this;
    GlobalLog.setLogger(this);

    final Function<CommandTree<CommandSender>, CommandExecutionCoordinator<CommandSender>>
        executionCoordinatorFunction =
        AsynchronousCommandExecutionCoordinator.<CommandSender>newBuilder().build();
    final Function<CommandSender, CommandSender> mapperFunction = Function.identity();
    try {
      manager =
          new PaperCommandManager<>(
              this, executionCoordinatorFunction, mapperFunction, mapperFunction);
    } catch (final Exception e) {
      fatal("Failed to initialize the command manager");
      this.getServer().getPluginManager().disablePlugin(this);
      return;
    }
    if (manager.hasCapability(CloudBukkitCapabilities.BRIGADIER)) {
      manager.registerBrigadier();
    }
    final Function<ParserParameters, CommandMeta> commandMetaFunction =
        p ->
            CommandMeta.simple()
                .with(
                    CommandMeta.DESCRIPTION,
                    p.get(StandardParameters.DESCRIPTION, "No description"))
                .build();
    AnnotationParser<CommandSender> annotationParser =
        new AnnotationParser<>(manager, CommandSender.class, commandMetaFunction);

    info(annotationParser.parse(new HikuKorobiCommands(this)));
    Bukkit.getPluginManager().registerEvents(new StartupListener(this), this);
    new LuckPermsListener(LuckPermsProvider.get());

    messengerProvider = new PaperMessengerProvider(this);
    messenger =
        SocketClient.Builder.build(
            messengerProvider,
            registry,
            getConfig().getString("socket_connect_to_ip"),
            getConfig().getInt("socket_connect_to_port"),
            getConfig().getString("secret_key"));

    new DebugProvider(messenger).register(registry);
    new KorobiProvider(messenger).register(registry);

    extensions.loadExtensions(messengerProvider);

    info("ShinKorobi has finished loading, %s extensions present", extensions.count());
  }

  public void info(@NotNull Object msg, Object... ext) {
    // HACK
    getComponentLogger().info(legacyLoggerSerializer.deserialize(String.format(msg.toString(), ext)));
  }

  public void debug(@NotNull Object msg, Object... ext) {
    // HACK
    if (getDebugMode())
      getComponentLogger().info(legacyLoggerSerializer.deserialize("[DEBUG] " + String.format(msg.toString(), ext)));
  }

  public void fatal(@NotNull Object msg, Object... ext) {
    // HACK
    getComponentLogger().error(legacyLoggerSerializer.deserialize(String.format(msg.toString(), ext)));
  }

  public void warn(@NotNull Object msg, Object... ext) {
    // HACK
    getComponentLogger().warn(legacyLoggerSerializer.deserialize(String.format(msg.toString(), ext)));
  }

  @Override
  public void onDisable() {
    messenger.closeIfRunning();
  }

  @Override
  public boolean getDebugMode() {
    return getConfig().getBoolean("debug", false);
  }
}
