package space.rymiel.shinkorobi.hiku.listener;

import net.luckperms.api.LuckPerms;
import net.luckperms.api.event.user.UserFirstLoginEvent;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;

public class LuckPermsListener {
  public LuckPermsListener(LuckPerms lp) {
    lp.getEventBus()
        .subscribe(
            ShinKorobiPaper.getInstance(),
            UserFirstLoginEvent.class,
            e -> ShinKorobiPaper.getInstance()
                .debug(
                    "[LuckPerms UserFirstLoginEvent] %s joined for the first time!",
                    e.getUsername()));
  }
}
