package space.rymiel.shinkorobi.hiku.listener;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerLoadEvent;
import space.rymiel.shinkorobi.common.provider.BuiltinMessage;
import space.rymiel.shinkorobi.common.provider.DebugMessage;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;

public record StartupListener(ShinKorobiPaper plugin) implements Listener {
  @EventHandler
  public void onServerLoad(ServerLoadEvent serverLoadEvent) {
    Bukkit.getScheduler().runTaskLater(plugin, this::doHandshake, 40);
  }

  private void doHandshake() {
    plugin
        .registry()
        .dispatch(
            BuiltinMessage.HANDSHAKE,
            new BuiltinMessage.Handshake(
                Bukkit.getName(),
                Bukkit.getIp(),
                Bukkit.getPort(),
                ShinKorobiPaper.getInstance().getConfig().getString("name", "")),
            response -> plugin.info("We got a handshake response %s", response));
    plugin.registry().dispatch(DebugMessage.STARTUP, "Start!");
  }
}
