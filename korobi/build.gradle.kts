plugins {
    id("net.kyori.blossom") version "1.3.0"
}

description = "shinkorobi"

dependencies {
    implementation("cloud.commandframework:cloud-paper:1.8.3")
    implementation("cloud.commandframework:cloud-annotations:1.8.3")
    implementation("org.spongepowered", "configurate-yaml", "4.1.2")
    implementation("com.h2database", "h2-mvstore", "2.1.210")
    compileOnly("com.velocitypowered", "velocity-api", "3.1.1")
    annotationProcessor("com.velocitypowered", "velocity-api", "3.1.1")
    compileOnly("io.papermc.paper:paper-api:1.19-R0.1-SNAPSHOT")
    compileOnly("me.clip:placeholderapi:2.10.9")
    compileOnly("net.luckperms:api:5.4")
    compileOnly("com.github.LeonMangler:SuperVanish:6.2.0") {
        isTransitive = false
    }
    compileOnly("net.essentialsx:EssentialsX:2.19.0-SNAPSHOT")
}

blossom {
    replaceTokenIn("src/main/java/space/rymiel/shinkorobi/taka/ShinKorobiVelocity.java")
    replaceTokenIn("src/main/java/space/rymiel/shinkorobi/common/messenger/protocol/SocketFormat.java")
    replaceToken("@version@", rootProject.version)
    replaceToken("@protocol_version@", rootProject.ext.get("protocolVersion"))
}

tasks {
    withType<ProcessResources> {
        filteringCharset = "UTF-8"
        filesMatching("*.yml") {
            expand("version" to rootProject.version)
        }
    }
}
