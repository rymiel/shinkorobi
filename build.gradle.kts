plugins {
  java
  id("com.github.johnrengelman.shadow") version "7.0.0"
}

val buildNum = System.getenv("CI_PIPELINE_IID") ?: "dirty"
group = "space.rymiel.shinkorobi"
version = "4.1.2-$buildNum"

ext {
  set("protocolVersion", 412)
}

java {
  sourceCompatibility = JavaVersion.VERSION_17
  targetCompatibility = JavaVersion.VERSION_17
}

subprojects {
  apply(plugin = "java")
  apply(plugin = "com.github.johnrengelman.shadow")

  java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
  }

  repositories {
    mavenCentral()
    maven("https://papermc.io/repo/repository/maven-public/")
    maven("https://oss.sonatype.org/content/repositories/snapshots")
    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
    maven("https://hub.spigotmc.org/nexus/content/groups/public/")
    maven("https://nexus.velocitypowered.com/repository/maven-public/")
    maven("https://repo.essentialsx.net/releases/")
    maven("https://repo.essentialsx.net/snapshots/")
    maven("https://repo.codemc.org/repository/maven-public")
    maven("https://repo.extendedclip.com/content/repositories/placeholderapi/")
    maven("https://jitpack.io")
  }

  tasks {
    shadowJar {
      dependencies {
        exclude(dependency("com.google.guava:"))
        exclude(dependency("com.google.errorprone:"))
        exclude(dependency("org.checkerframework:"))
        exclude(dependency("org.jetbrains:"))
        exclude(dependency("org.intellij:"))
      }

      relocate("cloud.commandframework", "${rootProject.group}.shade.cloud")
      relocate("io.leangen.geantyref", "${rootProject.group}.shade.typetoken")
      relocate("org.spongepowered.configurate", "${rootProject.group}.shade.configurate")
      relocate("org.yaml", "${rootProject.group}.shade.yaml")
      relocate("org.h2", "${rootProject.group}.shade.h2")

      archiveClassifier.set(null as String?)
      archiveFileName.set(project.description + "-" + rootProject.version + ".jar")
      destinationDirectory.set(rootProject.tasks.shadowJar.get().destinationDirectory.get())
    }
    build {
      dependsOn(shadowJar)
    }
  }
}
