package space.rymiel.shinkorobi.ext.tab.velocity;

import com.google.common.io.ByteArrayDataOutput;
import com.velocitypowered.api.event.PostOrder;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.DisconnectEvent;
import com.velocitypowered.api.event.player.ServerPostConnectEvent;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.player.TabListEntry;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import net.kyori.adventure.text.minimessage.MiniMessage;
import net.kyori.adventure.text.minimessage.tag.Tag;
import net.kyori.adventure.text.minimessage.tag.resolver.Placeholder;
import net.kyori.adventure.text.minimessage.tag.resolver.TagResolver;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.configurate.serialize.SerializationException;
import space.rymiel.shinkorobi.api.MessageListener;
import space.rymiel.shinkorobi.common.ConfigurationManager;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiCommunication;
import space.rymiel.shinkorobi.common.serializer.RecordSerializer;
import space.rymiel.shinkorobi.ext.JoinExtension;
import space.rymiel.shinkorobi.ext.TabExtension;
import space.rymiel.shinkorobi.ext.TabExtension.VanishState;
import space.rymiel.shinkorobi.taka.ShinKorobiVelocity;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.regex.Pattern;

@SuppressWarnings("UnstableApiUsage")
public final class VelocityListeners implements TabExtension.Listeners {
  private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("<%(.*?)%>");
  private final LoggingPlugin log;
  private final ShinKorobiVelocity parent;
  private final HashMap<@NotNull UUID, KnownState> knownStates = new HashMap<>();
  private final HashMap<@NotNull UUID, Component> knownBypass = new HashMap<>();
  private final ArrayList<String> knownPlaceholders = new ArrayList<>();
  private final HashSet<UUID> flagged = new HashSet<>();
  private TabConfig config;
  private Comparator<Player> comparator;

  private final LegacyComponentSerializer legacyComponentSerializer = LegacyComponentSerializer.builder().character(LegacyComponentSerializer.AMPERSAND_CHAR).hexColors().useUnusualXRepeatedCharacterHexFormat().build();
  private final MiniMessage miniMessage = MiniMessage.miniMessage();

  public static final class KnownState {
    public @NotNull HashMap<String, Component> tabNames;
    public @Nullable Component displayName;
    public @Nullable HashMap<String, String> replacements;
    public VanishState vanishState;

    public KnownState() {
      this.tabNames = new HashMap<>();
      this.displayName = null;
      this.replacements = null;
      this.vanishState = VanishState.Visible;
    }
  }

  public VelocityListeners(LoggingPlugin log, ShinKorobiVelocity parent) {
    this.log = log;
    this.parent = parent;
    reloadConfig();

    parent.getServer().getScheduler().buildTask(parent, () -> {
      var fullSet = new HashSet<UUID>(knownStates.size() + knownBypass.size());
      fullSet.addAll(knownStates.keySet());
      fullSet.addAll(knownBypass.keySet());
      for (var uuid : fullSet) {
        if (parent.getServer().getPlayer(uuid).isEmpty()) {
          erasePlayer(uuid);
        }
      }
    }).repeat(Duration.ofSeconds(2)).schedule();
  }

  private @NotNull KnownState stateOf(@NotNull UUID uuid) {
    return knownStates.computeIfAbsent(uuid, (ignored) -> new KnownState());
  }

  private void addAllPlaceholdersWithin(@Nullable String s) {
    if (s == null) return;
    var matcher = PLACEHOLDER_PATTERN.matcher(s);
    while (matcher.find())
      tryAddPlaceholder(matcher.group(1));
  }

  private void tryAddPlaceholder(String placeholder) {
    if (!knownPlaceholders.contains(placeholder)) knownPlaceholders.add(placeholder);
  }

  private @Nullable String getPlaceholder(UUID uuid, String placeholder) {
    var mapping = stateOf(uuid).replacements;
    if (mapping != null)
      return mapping.get(placeholder);
    return null;
  }

  private boolean isNumeric(@NotNull String str) {
    return !str.isEmpty() && str.chars().allMatch(chr -> chr >= '0' && chr <= '9');
  }

  @Contract(pure = true)
  private @NotNull Comparator<Player> placeholderComparator(String placeholder, boolean descending) {
    int invert = descending ? -1 : 1;
    return (a, b) -> {
      var valueA = Objects.requireNonNullElse(getPlaceholder(a.getUniqueId(), placeholder), "0");
      var valueB = Objects.requireNonNullElse(getPlaceholder(b.getUniqueId(), placeholder), "0");

      if (isNumeric(valueA) && isNumeric(valueB))
        return invert * Integer.compare(Integer.parseInt(valueA), Integer.parseInt(valueB));

      return invert * valueA.compareTo(valueB);
    };
  }

  public void reloadConfig() {
    log.debug("Reloading config");
    try {
      this.config = ConfigurationManager.objectFactory.get(TabConfig.class).load(parent.configNode().node("tab"));
      knownPlaceholders.clear();
      for (var group : this.config.groups()) {
        addAllPlaceholdersWithin(group.format());
        addAllPlaceholdersWithin(group.header());
        addAllPlaceholdersWithin(group.footer());
      }

      for (var replacement : this.config.replacements().entrySet()) {
        tryAddPlaceholder(replacement.getValue().placeholder());
      }

      this.comparator = (a, b) -> 0; // "Default" comparator
      for (var i : this.config.sortOrder()) {
        String key = i.getString();
        boolean descending = false;

        // yes this is very fragile
        if (i.isMap()) {
          var map = i.childrenMap();
          try {
            var firstEntry = map.entrySet().iterator().next();
            key = firstEntry.getValue().getString("");
            descending = Set.of("desc", "descending", "z-a").contains((String) firstEntry.getKey());
          } catch (RuntimeException ignored) {
          }
        }

        key = Objects.requireNonNullElse(key, "");

        if (key.startsWith("%") && key.endsWith("%")) {
          var placeholder = key.substring(1, key.length() - 1);
          tryAddPlaceholder(placeholder);
          this.comparator = this.comparator.thenComparing(placeholderComparator(placeholder, descending));
        } else if (key.equals("username")) {
          var usernameComparator = Comparator.comparing(Player::getUsername);
          if (descending)
            usernameComparator = usernameComparator.reversed();
          this.comparator = this.comparator.thenComparing(usernameComparator);
        } else {
          log.fatal("Unknown sort order entry '%s' (%s)", i, key);
        }
      }
      log.debug("Placeholders: %s", knownPlaceholders.toString());
    } catch (SerializationException e) {
      e.printStackTrace();
    }
  }

  private boolean checkCanSee(Player observer, Player target) {
    if (observer.equals(target))
      return true; // Player can always see themself
    var state = stateOf(target.getUniqueId()).vanishState;
    if (state == VanishState.Visible || state == VanishState.Silent) return true;

    return observer.hasPermission("pv.see"); // TODO(rymiel): put into config or something
  }

  private void upsertTabEntry(Player player, Player other, @Nullable Component displayName) {
    boolean canSee = checkCanSee(player, other);

    var tabList = player.getTabList();
    for (var i : tabList.getEntries()) {
      if (i.getProfile().getId().equals(other.getUniqueId())) {
        if (!canSee) {
          // The `other` player has vanished, remove them from tab
          tabList.removeEntry(other.getUniqueId());
          return;
        }
        if (displayName != null)
          i.setDisplayName(displayName);
        return;
      }
    }

    if (!canSee) {
      // A tab entry didn't exist, but since the player can't see the target, we aren't going to make one anyway
      return;
    }

    tabList.addEntry(
        TabListEntry.builder()
            .tabList(tabList)
            .profile(other.getGameProfile())
            .displayName(displayName)
            .build());
  }

  private String determinePlayerGroup(Player player) {
    return determinePlayerGroup(player, false, false);
  }

  private String determinePlayerGroup(Player player, boolean forHeader, boolean forFooter) {
    for (var group : this.config.groups()) {
      if (forHeader && group.header() == null) continue;
      if (forFooter && group.footer() == null) continue;
      if (player.hasPermission("shinkorobi.tab.group." + group.name()))
        return group.name();
    }
    return "default";
  }

  public void register() {
    parent.getServer().getEventManager().register(parent, this);
    JoinExtension.addChainCallback((msg) -> {
      stateOf(msg.uuid()).vanishState = msg.silent() ? VanishState.Hidden : VanishState.Visible; // I don't think you can join in "silent" state.

      if (flagged.remove(msg.uuid())) {
        this.parent.getServer().getPlayer(msg.uuid()).ifPresent(this::updateAllOtherEntries);
      }
    });
  }

  @MessageListener
  public void onSetVanishState(TabExtension.Message.SetVanishState msg) {
    stateOf(msg.player()).vanishState = VanishState.values()[msg.vanishState()];

    updateVisibility(msg.player());
  }

  @MessageListener
  public void onAskPlaceholders(TabExtension.Message.AskPlaceholders msg, KorobiCommunication com, ByteArrayDataOutput res) {
    res.write(RecordSerializer.serializeRecord(
        new TabExtension.Message.ListPlaceholders(
            knownPlaceholders.toArray(new String[0]),
            config.interval()))
    );
  }

  private Component legacyCompatibilityDeserialize(String s) {
    return legacyComponentSerializer.deserialize(s.replace(LegacyComponentSerializer.SECTION_CHAR, LegacyComponentSerializer.AMPERSAND_CHAR));
  }

  private void reprocessTabList(Player player, HashMap<String, String> lookup, @Nullable Component displayName, ByteArrayDataOutput res) {
    var uuid = player.getUniqueId();
    var username = player.getUsername();
    if (displayName != null) {
      stateOf(uuid).displayName = displayName;
    } else {
      displayName = Objects.requireNonNullElse(stateOf(uuid).displayName, Component.text(username));
    }

    var headerGroup = determinePlayerGroup(player, true, false);
    var footerGroup = determinePlayerGroup(player, false, true);
    Component header = Component.empty();
    Component footer = Component.empty();

    int unvanishedPlayerCount = 0;
    for (var i : knownStates.values()) {
      if (i.vanishState != VanishState.Hidden) unvanishedPlayerCount++;
    }

    var tagResolver = TagResolver.resolver(
        Placeholder.component("displayname", displayName),
        Placeholder.unparsed("username", username),
        Placeholder.component("player_count_including_vanished", Component.text(knownStates.size())),
        Placeholder.component("player_count_excluding_vanished", Component.text(unvanishedPlayerCount)),
        TagResolver.resolver("replace", (queue, ctx) -> {
          var replacementName = queue.pop().value();
          var replacement = config.replacements().get(replacementName);

          var placeholder = lookup.getOrDefault(replacement.placeholder(), "");
          var replaced = replacement.replace().get(placeholder);
          if (replaced != null) {
            return Tag.preProcessParsed(replaced);
          } else if (replacement.fallback() == null) {
            return Tag.inserting(legacyCompatibilityDeserialize(placeholder));
          } else {
            return Tag.preProcessParsed(replacement.fallback());
          }
        })
    );
    Function<@Nullable String, @NotNull Component> resolveAll = (s) -> {
      if (s == null) return Component.empty();
      var matcher = PLACEHOLDER_PATTERN.matcher(s);
      var replaced = matcher.replaceAll((match) -> miniMessage.serialize(legacyCompatibilityDeserialize(lookup.getOrDefault(match.group(1), ""))));
      return miniMessage.deserialize(replaced, tagResolver);
    };

    for (var group : this.config.groups()) {
      updateGroupTabName(player, group.name(), resolveAll.apply(group.format()));

      if (group.name().equals(headerGroup)) header = resolveAll.apply(group.header());
      if (group.name().equals(footerGroup)) footer = resolveAll.apply(group.footer());
    }

    player.sendPlayerListHeaderAndFooter(header, footer);
    List<Player> toSort = new ArrayList<>(this.parent.getServer().getAllPlayers());
    toSort.sort(this.comparator);
    List<UUID> uuidList = new ArrayList<>();
    List<String> nameList = new ArrayList<>();
    for (Player p : toSort) {
      uuidList.add(p.getUniqueId());
      nameList.add(p.getUsername());
    }
    var uuidOrder = uuidList.toArray(new UUID[0]);
    var nameOrder = nameList.toArray(new String[0]);

    res.write(RecordSerializer.serializeRecord(new TabExtension.Message.ListOrder(uuidOrder, nameOrder)));
  }

  @MessageListener
  public void onGivePlaceholders(TabExtension.Message.GivePlaceholders msg, KorobiCommunication com, ByteArrayDataOutput res) {
    var uuid = msg.player();
    var lookup = new HashMap<String, String>();
    for (int i = 0; i < knownPlaceholders.size(); i++) {
      lookup.put(knownPlaceholders.get(i), msg.replacements()[i]);
    }
    stateOf(uuid).replacements = lookup;

    var player = this.parent.getServer().getPlayer(uuid).orElse(null);
    if (player == null) return;

    reprocessTabList(player, lookup, msg.displayName(), res);
  }

  @MessageListener
  public void onRefreshPlaceholders(TabExtension.Message.RefreshPlaceholders msg, KorobiCommunication com, ByteArrayDataOutput res) {
    var uuid = msg.player();

    var lookup = Objects.requireNonNullElse(stateOf(uuid).replacements, new HashMap<String, String>());
    for (int i = 0; i < msg.keys().length; i++) {
      if (!knownPlaceholders.contains(msg.keys()[i])) continue;
      lookup.put(msg.keys()[i], msg.replacements()[i]);
    }
    stateOf(uuid).replacements = lookup;

    var player = this.parent.getServer().getPlayer(uuid).orElse(null);
    if (player == null) return;

    reprocessTabList(player, lookup, msg.hasNewDisplayName() ? msg.displayName() : null, res);
  }

  @MessageListener
  public void onAskDebugKnown(TabExtension.Message.AskDebugKnown msg, KorobiCommunication com, ByteArrayDataOutput res) {
    var c = Component.text();

    int j = 0;
    for (var i : knownStates.entrySet()) {
      if (j > 0) c.append(Component.newline());
      j++;
      var k = i.getKey();
      var v = i.getValue();
      c.append(Component.text(j + ". ", NamedTextColor.AQUA, TextDecoration.BOLD));
      c.append(Component.text(k.toString(), NamedTextColor.AQUA));
      c.append(Component.text(": "));
      c.append(Objects.requireNonNullElse(
          v.displayName,
          Component.text("(no display name set)", NamedTextColor.RED, TextDecoration.ITALIC)));
      c.append(Component.text(" (" + v.vanishState.toString() + ") ", NamedTextColor.LIGHT_PURPLE, TextDecoration.BOLD));
      c.append(Component.space());
      c.append(Objects.requireNonNullElse(
          this.parent.getServer().getPlayer(k).map(p -> Component.text(p.toString())).orElse(null),
          Component.text("(Error!)", NamedTextColor.RED, TextDecoration.ITALIC)
      ));
    }

    res.write(RecordSerializer.serializeRecord(new TabExtension.Message.DebugKnown(c.build())));
  }

  @Subscribe(order = PostOrder.FIRST)
  public void disconnectedEvent(DisconnectEvent event) {
    erasePlayer(event.getPlayer().getUniqueId());
  }

  private void erasePlayer(UUID uuid) {
    this.knownStates.remove(uuid);
    this.knownBypass.remove(uuid);

    for (var other : this.parent.getServer().getAllPlayers()) {
      other.getTabList().removeEntry(uuid);
    }
  }

  @Subscribe(order = PostOrder.FIRST)
  public void connectedEvent(ServerPostConnectEvent event) {
    var player = event.getPlayer();
    var server = player.getCurrentServer().orElseThrow();
    for (var bypass : this.config.bypass()) {
      if (bypass.name().equals(server.getServerInfo().getName())) {
        var displayName = MiniMessage.miniMessage().deserialize(bypass.format(), Placeholder.unparsed("username", player.getUsername()));
        for (var other : this.parent.getServer().getAllPlayers()) {
          var tabList = other.getTabList();
          tabList.addEntry(
              TabListEntry.builder()
                  .tabList(tabList)
                  .profile(player.getGameProfile())
                  .displayName(displayName)
                  .build());
        }
        this.knownBypass.put(player.getUniqueId(), displayName);
        return; // This player won't be flagged
      }
    }
    flagged.add(player.getUniqueId());
  }

  private void updateGroupTabName(@NotNull Player player, @NotNull String group, @NotNull Component component) {
    stateOf(player.getUniqueId()).tabNames.put(group, component);
    for (var other : this.parent.getServer().getAllPlayers()) {
      if (determinePlayerGroup(other).equals(group)) {
        upsertTabEntry(other, player, component);
      }
    }
  }

  private void updateVisibility(@NotNull UUID uuid) {
    this.parent.getServer().getPlayer(uuid).ifPresent(player -> {
      for (var other : this.parent.getServer().getAllPlayers()) {
        upsertTabEntry(other, player, null);
      }
    });
  }

  private void updateAllOtherEntries(Player player) {
    for (var i : knownStates.entrySet()) {
      if (i.getKey().equals(player.getUniqueId())) continue;
      String group = determinePlayerGroup(player);
      Component name = i.getValue().tabNames.get(group);
      if (name == null) continue;

      var target = this.parent.getServer().getPlayer(i.getKey());
      target.ifPresent(other -> upsertTabEntry(player, other, name));
    }

    var tabList = player.getTabList();
    for (var i : knownBypass.entrySet()) {
      this.parent.getServer().getPlayer(i.getKey()).ifPresent(other ->
          tabList.addEntry(
              TabListEntry.builder()
                  .tabList(tabList)
                  .profile(other.getGameProfile())
                  .displayName(i.getValue())
                  .build())
      );
    }
  }
}
