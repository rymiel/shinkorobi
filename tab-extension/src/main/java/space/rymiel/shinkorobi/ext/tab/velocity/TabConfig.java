package space.rymiel.shinkorobi.ext.tab.velocity;

import org.jetbrains.annotations.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;

import java.util.ArrayList;
import java.util.HashMap;

@ConfigSerializable
public record TabConfig(
    long interval,
    ArrayList<TabGroup> groups,
    HashMap<String, PlaceholderReplacement> replacements,
    ArrayList<ConfigurationNode> sortOrder,
    ArrayList<Bypass> bypass
) {
  @ConfigSerializable
  public record PlaceholderReplacement(
      String placeholder,
      HashMap<String, String> replace,
      @Nullable String fallback
  ) {
  }

  @ConfigSerializable
  public record TabGroup(
      String name,
      String format,
      @Nullable String header,
      @Nullable String footer
  ) {
  }

  @ConfigSerializable
  public record Bypass(
      String name,
      String format
  ) {
  }
}
