package space.rymiel.shinkorobi.ext.tab.paper;

import de.myzelyam.api.vanish.PlayerHideEvent;
import de.myzelyam.api.vanish.PlayerShowEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.ext.TabExtension;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;

public class VanishListeners implements Listener {
  private final ShinKorobiPaper parent;

  public VanishListeners(ShinKorobiPaper parent) {
    this.parent = parent;
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onVanish(PlayerHideEvent playerHideEvent) {
    var newState = playerHideEvent.isSilent() ? TabExtension.VanishState.Silent : TabExtension.VanishState.Hidden;
    Player p = playerHideEvent.getPlayer();
    parent.registry().dispatch(new TabExtension.Message.SetVanishState(p.getUniqueId(), newState.ordinal()));
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onUnVanish(PlayerShowEvent playerShowEvent) {
    Player p = playerShowEvent.getPlayer();
    parent.registry().dispatch(new TabExtension.Message.SetVanishState(p.getUniqueId(), TabExtension.VanishState.Visible.ordinal()));
  }
}
