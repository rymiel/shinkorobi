package space.rymiel.shinkorobi.ext.tab.paper;

import com.google.common.io.ByteStreams;
import me.clip.placeholderapi.PlaceholderAPI;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerLoadEvent;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.Scoreboard;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import space.rymiel.shinkorobi.common.LoggingPlugin;
import space.rymiel.shinkorobi.common.messenger.protocol.KorobiCommunication;
import space.rymiel.shinkorobi.common.serializer.RecordSerializer;
import space.rymiel.shinkorobi.ext.JoinExtension;
import space.rymiel.shinkorobi.ext.TabExtension;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

@SuppressWarnings("UnstableApiUsage")
public final class PaperListeners implements TabExtension.Listeners, Listener {

  private static final String TEAM_PREFIX = "shinkorobi-";
  private final TabExtension ext;
  private final LoggingPlugin log;
  private final ShinKorobiPaper parent;

  private Scoreboard orderedScoreboard = null;

  private String[] knownPlaceholders = new String[0];
  private final HashMap<Player, String[]> activePlayers = new HashMap<>();
  private final HashMap<Player, Component> activeDisplayNames = new HashMap<>();
  private UUID[] knownUuids = new UUID[0];

  private long interval = 20;
  private BukkitTask refreshTask = null;

  public PaperListeners(TabExtension ext, LoggingPlugin log, ShinKorobiPaper parent) {
    this.ext = ext;
    this.log = log;
    this.parent = parent;
  }

  @Override
  public void reloadConfig() {
    Bukkit.getOnlinePlayers().forEach(player -> parent
        .registry()
        .dispatch(new TabExtension.Message.AskPlaceholders(),
            res -> askPlaceholderCallback(player, res)));
  }

  public void register() {
    JoinExtension.addChainCallback((join) -> parent
        .registry()
        .dispatch(new TabExtension.Message.AskPlaceholders(),
            res -> askPlaceholderCallback(Bukkit.getPlayer(join.uuid()), res)));
    Bukkit.getPluginManager().registerEvents(this, parent);
    rescheduleRefreshTask(interval);
  }

  private void rescheduleRefreshTask(long interval) {
    this.interval = interval;
    if (refreshTask != null) {
      refreshTask.cancel();
    }
    refreshTask = Bukkit.getScheduler().runTaskTimerAsynchronously(parent, this::refreshPlaceholders, interval, interval);
  }

  private String @NotNull [] replaceAllPlaceholders(@NotNull Player player, String @NotNull [] input) {
    var output = new String[input.length];

    for (int i = 0; i < input.length; i++) {
      String placeholderInput = "%" + input[i] + "%";
      var replaced = PlaceholderAPI.setPlaceholders(player, placeholderInput);
      if (replaced.equals(placeholderInput))
        replaced = "";
      output[i] = replaced;
    }

    return output;
  }

  private void askPlaceholderCallback(@Nullable Player player, @NotNull KorobiCommunication res) {
    // Don't respond if the player somehow left the server already
    if (player == null || !player.isOnline())
      return;

    var list = RecordSerializer.deserializeRecord(
        ByteStreams.newDataInput(res.body()),
        TabExtension.Message.ListPlaceholders.class);

    if (list.interval() != interval)
      rescheduleRefreshTask(list.interval());

    var replaced = this.replaceAllPlaceholders(player, list.placeholders());
    if (!Arrays.equals(list.placeholders(), knownPlaceholders)) {
      activePlayers.clear();
    }
    knownPlaceholders = list.placeholders();
    activePlayers.put(player, replaced);
    activeDisplayNames.put(player, player.displayName());

    parent.registry().dispatch(new TabExtension.Message.GivePlaceholders(player.getUniqueId(), player.displayName(), replaced),
        res1 -> Bukkit.getScheduler().runTask(parent, () -> updatePlayerOrderTeams(res1))
    );
  }

  private void updatePlayerOrderTeams(@NotNull KorobiCommunication res) {
    // Empty response, I don't really know why this happens but it does
    if (res.body().length == 0) return;

    var order = RecordSerializer.deserializeRecord(
        ByteStreams.newDataInput(res.body()),
        TabExtension.Message.ListOrder.class);
    var uuids = order.uuids();
    var names = order.names();

    if (Arrays.equals(uuids, knownUuids)) return;
    knownUuids = uuids;

    if (orderedScoreboard == null) {
      orderedScoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    }

    for (int i = 0; i < uuids.length; i++) {
      var player = Bukkit.getOfflinePlayer(uuids[i]);
      if (player.getName() == null) {
        log.warn("%s has a null name, we were given %s as a backup", uuids[i], names[i]);
      }

      var desiredTeamName = "%s%05d".formatted(TEAM_PREFIX, i);
      var existingTeam = orderedScoreboard.getPlayerTeam(player);
      if (existingTeam != null) {
        var existingName = existingTeam.getName();
        // TODO: check if we made the team
        if (existingName.equals(desiredTeamName)) {
          // Don't need to touch this player further
          continue;
        } else if (existingName.startsWith(TEAM_PREFIX)) {
          existingTeam.unregister();
        } else {
          continue; // Can we do anything else?
        }
      }

      var existingTeamByName = orderedScoreboard.getTeam(desiredTeamName);
      if (existingTeamByName != null)
        existingTeamByName.unregister();

      var newTeam = orderedScoreboard.registerNewTeam(desiredTeamName);
      newTeam.addEntry(names[i]);
    }

    for (var onlinePlayer : Bukkit.getOnlinePlayers()) {
      onlinePlayer.setScoreboard(orderedScoreboard);
    }
  }

  private void refreshPlaceholders() {
    var gonePlayers = new ArrayList<Player>();
    for (var e : activePlayers.entrySet()) {
      var mismatchedKeys = new ArrayList<String>();
      var mismatchedValues = new ArrayList<String>();
      Component mismatchedDisplayName = null;

      var p = e.getKey();
      if (!p.isOnline()) {
        // Player is gone, I guess
        gonePlayers.add(p);
        continue;
      }
      var prev = e.getValue();
      var next = replaceAllPlaceholders(p, knownPlaceholders);
      if (prev.length != next.length && prev.length != knownPlaceholders.length) continue;

      for (int i = 0; i < next.length; i++) {
        if (!next[i].equals(prev[i])) {
          mismatchedKeys.add(knownPlaceholders[i]);
          mismatchedValues.add(next[i]);
        }
      }
      activePlayers.put(p, next);

      if (!p.displayName().equals(activeDisplayNames.get(p)))
        mismatchedDisplayName = p.displayName();

      activeDisplayNames.put(p, p.displayName());

      if (mismatchedKeys.isEmpty() && mismatchedDisplayName == null) continue;

      parent.registry().dispatch(
          new TabExtension.Message.RefreshPlaceholders(
              p.getUniqueId(),
              mismatchedDisplayName != null,
              Objects.requireNonNullElse(mismatchedDisplayName, Component.empty()),
              mismatchedKeys.toArray(new String[0]),
              mismatchedValues.toArray(new String[0])),
          res -> Bukkit.getScheduler().runTask(parent, () -> updatePlayerOrderTeams(res)));
    }
    for (var e : gonePlayers) {
      activePlayers.remove(e);
    }
  }

  @EventHandler(priority = EventPriority.HIGHEST)
  public void onPlayerQuit(PlayerQuitEvent playerQuitEvent) {
    activePlayers.remove(playerQuitEvent.getPlayer());
  }

  @EventHandler
  public void onServerLoad(ServerLoadEvent serverLoadEvent) {
    if (Bukkit.getPluginManager().isPluginEnabled("SuperVanish")
        || Bukkit.getPluginManager().isPluginEnabled("PremiumVanish")) {
      log.info("Using vanish listener");
      Bukkit.getPluginManager().registerEvents(new VanishListeners(parent), parent);
      log.info("Registered");
    }
  }
}
