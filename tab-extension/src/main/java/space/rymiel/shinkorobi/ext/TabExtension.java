package space.rymiel.shinkorobi.ext;

import cloud.commandframework.paper.PaperCommandManager;
import com.google.common.io.ByteStreams;
import net.kyori.adventure.text.Component;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.Nullable;
import space.rymiel.shinkorobi.api.AMessage;
import space.rymiel.shinkorobi.api.KorobiExtension;
import space.rymiel.shinkorobi.common.provider.common.IMessageEnum;
import space.rymiel.shinkorobi.common.serializer.RecordSerializer;
import space.rymiel.shinkorobi.ext.tab.paper.PaperListeners;
import space.rymiel.shinkorobi.ext.tab.velocity.VelocityListeners;
import space.rymiel.shinkorobi.hiku.PaperMessengerProvider;
import space.rymiel.shinkorobi.hiku.ShinKorobiPaper;
import space.rymiel.shinkorobi.taka.ShinKorobiVelocity;
import space.rymiel.shinkorobi.taka.VelocityMessengerProvider;

import java.util.UUID;

public class TabExtension extends KorobiExtension {
  public TabExtension() {
    super("tab");
  }

  private @Nullable Listeners listeners = null;

  @Override
  public void onEnable() {
    if (parent.messengerProvider() instanceof PaperMessengerProvider) {
      var korobiPaper = (ShinKorobiPaper) parent;
      log.info("We're on paper! %s", korobiPaper);

      PaperCommandManager<CommandSender> cmd = korobiPaper.commandManager();
      cmd.command(
          cmd.commandBuilder("korobi")
              .literal("tab")
              .literal("debug")
              .handler((ctx) -> parent.registry().dispatch(new Message.AskDebugKnown(), (res) -> {
                var message = RecordSerializer.deserializeRecord(
                    ByteStreams.newDataInput(res.body()),
                    TabExtension.Message.DebugKnown.class);

                ctx.getSender().sendMessage(message.message());
                korobiPaper.getComponentLogger().info(message.message());
              }))
              .permission("korobi.tab.debug")
      );

      this.listeners = new PaperListeners(this, this.log, korobiPaper);
      listeners.register();

      register(Message.class).declareAll(Message.class);
    } else if (parent.messengerProvider() instanceof VelocityMessengerProvider) {
      var korobiVelocity = (ShinKorobiVelocity) parent;
      log.info("We're on velocity! %s", korobiVelocity);

      this.listeners = new VelocityListeners(this.log, korobiVelocity);
      listeners.register();

      register(Message.class).listenAll(listeners);
    } else {
      log.fatal("Unknown platform %s %s", parent.getClass(), parent);
    }
  }

  @Override
  public void onReloadConfig() {
    if (this.listeners != null) {
      this.listeners.reloadConfig();
    }
  }

  public enum Message implements IMessageEnum {
    ASK_PLACEHOLDERS,
    GIVE_PLACEHOLDERS,
    REFRESH_PLACEHOLDERS,
    SET_VANISH_STATE,
    ASK_DEBUG_KNOWN;

    @AMessage(i = Message.class, name = "ASK_PLACEHOLDERS")
    public record AskPlaceholders() {
    }

    @AMessage(i = Message.class, name = "GIVE_PLACEHOLDERS")
    public record GivePlaceholders(UUID player, Component displayName, String[] replacements) {
    }

    @AMessage(i = Message.class, name = "REFRESH_PLACEHOLDERS")
    public record RefreshPlaceholders(UUID player, boolean hasNewDisplayName, Component displayName, String[] keys,
                                      String[] replacements) {
    }

    @AMessage(i = Message.class, name = "SET_VANISH_STATE")
    public record SetVanishState(UUID player, int vanishState) {
    }

    @AMessage(i = Message.class, name = "ASK_DEBUG_KNOWN")
    public record AskDebugKnown() {
    }

    public record ListPlaceholders(String[] placeholders, long interval) {
    }

    public record ListOrder(UUID[] uuids, String[] names) {
    }

    public record DebugKnown(Component message) {
    }
  }

  public interface Listeners {
    void reloadConfig();

    void register();
  }

  public enum VanishState {
    Visible, Hidden, Silent
  }
}
