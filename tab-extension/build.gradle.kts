description = "korobi-tab-extension"

dependencies {
    implementation("org.spongepowered", "configurate-yaml", "4.1.2")
    compileOnly(project(":korobi"))
    compileOnly(project(":join-extension"))
    compileOnly("me.clip:placeholderapi:2.10.9")
    compileOnly("io.papermc.paper:paper-api:1.19-R0.1-SNAPSHOT")
    compileOnly("com.velocitypowered", "velocity-api", "3.1.1")
    compileOnly("cloud.commandframework:cloud-paper:1.8.3")
    compileOnly("com.github.LeonMangler:SuperVanish:6.2.0") {
        isTransitive = false
    }
}
